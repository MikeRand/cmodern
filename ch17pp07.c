#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_REMIND 60
#define MSG_LEN 60

typedef struct vstring {
	int len;
	char msg[];
} vstring;

int read_line(char str[], int n);

int main(void) {
	vstring *reminders[MAX_REMIND];
	char day_str[3], msg_str[MSG_LEN+1];
	int day, i, j, num_remind = 0;
	
	for (;;) {
		if (num_remind == MAX_REMIND) {
			printf("-- No space left --\n");
			break;
		}
		
		printf("Enter day and reminder: ");
		if (scanf("%2d", &day) != 1)
			exit(EXIT_FAILURE);
		if (day == 0)
			break;
		sprintf(day_str, "%2d", day);
		read_line(msg_str, MSG_LEN);
		
		for (i = 0; i < num_remind; i++)
			if (strcmp(day_str, reminders[i]->msg) < 0)
				break;
		for (j = num_remind; j > i; j--)
			reminders[j] = reminders[j-1];
			
		reminders[i] = malloc(sizeof(vstring) + 2 + strlen(msg_str) + 1);
		reminders[i]->len = 2 + strlen(msg_str);
		if (reminders[i] == NULL) {
			printf("-- No space left --\n");
			break;
		}
		
		strcpy(reminders[i]->msg, day_str);
		strcat(reminders[i]->msg, msg_str);
		
		num_remind++;
	}
	
	printf("\nDay Reminder\n");
	for (i = 0; i < num_remind; i++)
		printf("%s\n", reminders[i]->msg);
		
	exit(EXIT_SUCCESS);
}

int read_line(char str[], int n) {
	char ch;
	int i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
