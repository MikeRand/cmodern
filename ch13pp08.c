// Convert phone number

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define WORD_LEN 30

int compute_scrabble_value(const char *word);
int read_line(char str[], int n);

int main(void) {

	char word[WORD_LEN+1];
	
	printf("Enter a word: ");
	read_line(word, WORD_LEN);
	printf("Scrabble value: %d\n", compute_scrabble_value(word));
	exit(EXIT_SUCCESS);
}

int compute_scrabble_value(const char *word) {
	int word_value = 0;
	const char *chp;
  
	for (chp = word; *chp; chp++) {
		switch (toupper(*chp)) {
			case 'Q': case 'Z':
				word_value += 10;
				break;
			case 'J': case 'X': 
				word_value += 8;
				break;
			case 'K':
				word_value += 5;
				break;
			case 'F': case 'H': case 'V': case 'W': case 'Y':
				word_value += 4;
				break;
			case 'B': case 'C': case 'M': case 'P':
				word_value += 3;
				break;
			case 'D': case 'G':
				word_value += 2;
				break;
			default:
				word_value += 1;
				break;
		}
	}
	return word_value;
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
