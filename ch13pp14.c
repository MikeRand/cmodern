// Anagrams

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 20

bool are_anagrams(const char *word1, const char *word2);
int read_line(char str[], int n);

int main(void) {

	char word1[MAX_LENGTH + 1], word2[MAX_LENGTH + 1];

	printf("Enter first word: ");
	read_line(word1, MAX_LENGTH);
	printf("Enter second word: ");
	read_line(word2, MAX_LENGTH);

	if (are_anagrams(word1, word2))
		printf("The words are anagrams.\n");
	else
		printf("The words are not anagrams.\n");

	exit(EXIT_SUCCESS);
}  
 
bool are_anagrams(const char *word1, const char *word2) { 
	char letters[26] = {0};
	const char *word1_ptr, *word2_ptr;

	for (word1_ptr = word1; *word1_ptr; word1_ptr++)
		letters[tolower(*word1_ptr) - 'a']++;
	
	for (word2_ptr = word2; *word2_ptr; word2_ptr++)
		letters[tolower(*word2_ptr) - 'a']--;

	for (int i = 0; i < 26; i++)
		if (letters[i] != 0)
			return false;
	return true;
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
