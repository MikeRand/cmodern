#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "ch19pp07_queueADT.h"

typedef struct node_type {
	Item data;
	struct node_type *next;
	struct node_type *prev;
} Node;
 
struct queue_type {
	struct node_type *sentinel;
	int len;
};

static void terminate(char *msg) {
	printf("%s\n", msg);
	exit(EXIT_FAILURE);
}

Queue create(void) {
	Queue new_queue;
	new_queue = malloc(sizeof(struct queue_type));
	if (new_queue == NULL)
		terminate("Error in create: malloc failed.");
	new_queue->sentinel = malloc(sizeof(Node));
	if (new_queue->sentinel == NULL) {
		free(new_queue);
		terminate("Error in create: malloc failed.");
	}
	new_queue->sentinel->next = new_queue->sentinel;
	new_queue->sentinel->prev = new_queue->sentinel;
	new_queue->len = 0;
	return new_queue;
}

void destroy(Queue q) {
	make_empty(q);
	free(q);
}

void make_empty(Queue q) {
	while (!is_empty(q))
		pop(q);
}

bool is_empty(Queue q) {
	return q->len == 0;
}

bool is_full(Queue q) {
	(void) q;
	return false;
}

void push(Queue q, Item i) {
	Node *new_node;
	new_node = malloc(sizeof(Node));
	if (new_node == NULL)
		terminate("error in push: queue is full.");
	new_node->data = i;
	new_node->next = q->sentinel;
	new_node->prev = q->sentinel->prev;
	q->sentinel->prev->next = new_node;
	q->sentinel->prev = new_node;
	q->len++;
}

Item pop(Queue q) {
	Item i;
	Node *old_node;
	if (is_empty(q))
		terminate("error in pop: queue is empty.");
	old_node = q->sentinel->next;
	i = old_node->data;
	q->sentinel->next = old_node->next;
	old_node->next->prev = q->sentinel;
	free(old_node);
	q->len--;
	return i;
}

Item first(Queue q) {
	if (is_empty(q))
		terminate("error in first: queue is empty.");
	return q->sentinel->next->data;
}

Item last(Queue q) {
	if (is_empty(q))
		terminate("error in last: queue is empty.");
	return q->sentinel->prev->data;
}

int length(Queue q) {
	return q->len;
}
