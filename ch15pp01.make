ch15pp01: justify.o word.o ch15pp01.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch15pp01 justify.o word.o ch15pp01.o

justify.o: justify.c word.h line.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c justify.c

word.o: word.c word.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c word.c

ch15pp01.o: ch15pp01.c line.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c ch15pp01.c
