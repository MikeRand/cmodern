// Remember to use const and pass-by-copy to protect values.

#include <stdio.h>
#include <stdlib.h>
#include "readline.h"

#define NAME_LEN 25
#define MAX_PARTS 100

typedef struct part {
	int number;
	char name[NAME_LEN+1];
	int on_hand;
} part;

int find_part(int number, const part *inventory, int num_parts);
void insert(part *inventory, int *num_parts);
void search(const part *inventory, int num_parts);
void update(part *inventory, int num_parts);
void print(const part *inventory, int num_parts);

int main(void) {
	part inventory[MAX_PARTS];
	int num_parts = 0;
	char code;
	
	for (;;) {
		printf("Enter operation code: ");
		if (scanf(" %c", &code) != 1)
			exit(EXIT_FAILURE);
		while (getchar() != '\n')
			;
		switch (code) {
			case 'i': insert(inventory, &num_parts); break;
			case 's': search(inventory, num_parts); break;
			case 'u': update(inventory, num_parts); break;
			case 'p': print(inventory, num_parts); break;
			case 'q': exit(EXIT_SUCCESS);
			default: printf("Illegal code ... i/s/u/p/q only.\n");
		}
	printf("\n");
	}
}

int find_part(int number, const part *inventory, int num_parts) {
	int i;
	for (i = 0; i < num_parts; i++)
		if (inventory[i].number == number)
			return i;
	return -1;
}

void insert(part *inventory, int *num_parts) {
	int part_number;
	if (*num_parts == MAX_PARTS) {
		printf("Database is full; can't add more parts.\n");
		return;
	}
	printf("Enter part number: ");
	if (scanf("%d", &part_number) != 1)
		exit(EXIT_FAILURE);
	if (find_part(part_number, inventory, *num_parts) >= 0) {
		printf("Part already exists.\n");
		return;
	}
	inventory[*num_parts].number = part_number;
	printf("Enter part name: ");
	read_line(inventory[*num_parts].name, NAME_LEN);
	printf("Enter quantity on hand: ");
	if (scanf("%d", &inventory[*num_parts].on_hand) != 1)
		exit(EXIT_FAILURE);
	(void) (*num_parts)++;
}

void search(const part *inventory, int num_parts) {
	int i, number;
	printf("Enter part number: ");
	if (scanf("%d", &number) != 1)
		exit(EXIT_FAILURE);
	i = find_part(number, inventory,  num_parts);
	if (i >= 0) {
		printf("Part name: %s\n", inventory[i].name);
		printf("Quantity on hand: %d\n", inventory[i].on_hand);
	} else
		printf("Part not found.\n");
}

void update(part *inventory, int num_parts) {
	int i, number, change;
	
	printf("Enter part number: ");
	if (scanf("%d", &number) != 1)
		exit(EXIT_FAILURE);
	i = find_part(number, inventory, num_parts);
	if (i >= 0) {
		printf("Enter change in quantity on hand: ");
		if (scanf("%d", &change) != 1)
			exit(EXIT_FAILURE);
		inventory[i].on_hand += change;
	} else
		printf("Part not found.\n");
}

void print(const part *inventory, int num_parts) {
	int i;
	
	printf("Part Number      Part Name                  "
				 "Quantity on Hand\n");
	for (i = 0; i < num_parts; i++)
		printf("%7d          %-25s%11d\n", inventory[i].number,
					 inventory[i].name, inventory[i].on_hand);
}
