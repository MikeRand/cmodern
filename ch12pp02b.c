// Check palindrome

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define LEN 80

bool is_palindrome(char *msg, int n);

int main(void) {
	int i = 0;
	char msg[LEN];
	
	printf("Enter a message: ");
	while ((msg[i++] = getchar()) != '\n')
		;
	if (is_palindrome(msg, i))
		printf("Palindrome");
	else
		printf("Not a palindrome");
	printf("\n");
	
	exit(EXIT_SUCCESS);
}

bool is_palindrome(char *msg, int n) {
	char *i, *j;
	for (i = msg, j = (msg + n - 1); i <= j;) {
		if (isalpha(*i) && isalpha(*j) && toupper(*i) != toupper(*j))
			return false;
		else if (isalpha(*i) && isalpha(*j)) {
			i++;
			j--;
		}
		else if (!isalpha(*i))
			i++;
		if (!isalpha(*j))
			j--;
	}
	return true;
}

