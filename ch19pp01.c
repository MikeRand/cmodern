// Match braces

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "stackADT.h"

int main(void) {
  int ch;
	Stack s;
	
	s = create();
  
  printf("Enter parentheses and/or braces: ");
  while ((ch = getchar()) != '\n') {
    if (ch == '(' || ch == '{')
      push(s, ch);
    else if (ch == ')') {
      if (pop(s) != '(') {
	printf("Parentheses/braces are not nested properly.\n");
	exit(EXIT_SUCCESS);
      }
    } else if (ch == '}') {
      if (pop(s) != '{') {
	printf("Parentheses/braces are not nested properly.\n");
	exit(EXIT_SUCCESS);
      }
    }
  }
  
  if (is_empty(s))
    printf("Parentheses/braces are nested properly.\n");
  else
    printf("Parentheses/braces are not nested properly.\n");

  exit(EXIT_SUCCESS);
}
