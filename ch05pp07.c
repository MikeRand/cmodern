#include <stdio.h>

int main(void) {
  int a, b, c, d, l1, l2, s1, s2, l, s;

  printf("Enter four integers, separated by spaces: ");
  scanf("%d %d %d %d", &a, &b, &c, &d);

  if (a < b) {
    s1 = a;
    l1 = b;
  } else {
    s1 = b;
    l1 = a;
  }

  if (c < d) {
    s2 = c;
    l2 = d;
  } else {
    s2 = d;
    l2 = c;
  }
  
  if (s1 < s2) {
    s = s1;
  } else {
    s = s2;
  }
  
  if (l1 > l2) {
    l = l1;
  } else {
    l = l2;
  }

  printf("Largest: %d\n", l);
  printf("Smallest: %d\n", s);

  return 0;
}

