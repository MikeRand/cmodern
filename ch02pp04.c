#include <stdio.h>

#define RATE 0.05f

int main(void) {
  float amt;

  printf("Enter an amount: ");
  scanf("%f",&amt);
  
  printf("With tax added: $%.2f\n", amt * (1 + RATE));

  return 0;
}
