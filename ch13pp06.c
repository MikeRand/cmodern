// Checks planet names.

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_PLANETS 9

int s_equal(const char *a, const char *b);

int main(int argc, char *argv[]) {
	char *planets[] = {"Mercury", "Venus", "Earth",
										 "Mars", "Jupiter", "Saturn",
										 "Uranus", "Neptune", "Pluto"};

	int i, j;
	
	for (i = 1; i < argc; i++) {
		for (j = 0; j < NUM_PLANETS; j++)
			if (s_equal(argv[i], planets[j]) == 1) {
				printf("%s is planet %d\n", argv[i], j + 1);
				break;
			}
		if (j == NUM_PLANETS)
			printf("%s is not a planet\n", argv[i]);
	}
	
	exit(EXIT_SUCCESS);
}

int s_equal(const char *a, const char *b) {
	const char *p = a;
	const char *q = b;
	for (; *p && *q;)
		if (toupper(*p++) != toupper(*q++))
			return 0;
	return 1;
}
