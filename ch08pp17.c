// Magic square

#include <stdio.h>
#include <stdlib.h>

int main(void) {
  int n, i, row, col, next_row, next_col;

  printf("Enter size of magic square: ");
  scanf("%d", &n);

  int square[n][n];

  for (int r = 0; r < n; r++) {
    for (int c = 0; c < n; c++) {
      square[r][c] = 0;
    }
  }

  row = 0;
  col = n / 2;

  for (i = 1; i <= (n * n); i++) {
      square[row][col] = i;
      if (row - 1 < 0) {
	next_row = n - 1;
      } else {
	next_row = row - 1;
      }
      if (col + 1 == n) {
	next_col = 0;
      } else {
	next_col = col + 1;
      }

      if (square[next_row][next_col] == 0) {
	row = next_row;
	col = next_col;
      } else {
	if (row + 1 == n) {
	  row = 0;
	} else {
	  row = (row + 1);
	}
      }
  }

  for (int r = 0; r < n; r++) {
    for (int c = 0; c < n; c++) {
      printf("%5d", square[r][c]);
    }
    printf("\n");
  }
  exit(EXIT_SUCCESS);
}
