#include <stdio.h>
#include <stdlib.h>

int main(void) {
  
  int i, digit;
  int digits[10] = {0};
  long n;

  printf("Enter a number: ");
  scanf("%ld", &n);

  while (n > 0) {
    digit = n % 10;
    digits[digit]++;
    n /= 10;
  }

  printf("Repeated digit(s):");
  for (i = 0; i < 10; i++) {
    if (digits[i] > 1) {
      printf(" %d", i);
    }
  }
  printf("\n");
    
  exit(EXIT_SUCCESS);
}
  
  
