#include <stdio.h>

int main(void) {
  int i, n;

  printf("Enter number of entries: ");
  scanf("%d", &n);
  getchar();  // Need this or the newline from the scanf will satisfy
              // the first getchar() below (i.e. no stop @ 24).
  
  for (i = 1; i <= n; i++) {
    printf("%10d%10d\n", i, i * i);
    if (i % 24 == 0) {
      printf("Press Enter to continue...");
      getchar();
    }
  }
  return 0;
}
