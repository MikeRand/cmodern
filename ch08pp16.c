#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 20

int main(void) {

  int i;
  char ch;
  char letters[26] = {0};

  printf("Enter first word: ");
  
  for (i = 0; (ch = getchar()) != '\n'; i++) {
    letters[tolower(ch) - 'a']++;
  }

  printf("Enter second word: ");
  
  for (i = 0; (ch = getchar()) != '\n'; i++) {
    letters[tolower(ch) - 'a']--;
  }

  for (i = 0; i < 26; i++) {
    if (letters[i] != 0) {
      break;
    }
  }
  
  if (i == 26) {
    printf("The words are anagrams.\n");
  } else {
    printf("The words are not anagrams.\n");
  }

  exit(EXIT_SUCCESS);
}
    
    
    
  
