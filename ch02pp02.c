#include <stdio.h>
#include <math.h>

int main(void) {
  int r = 10;
  float v;

  v = 4.0f / 3.0f * M_PI * r * r * r;

  printf("A sphere of radius %d has a volume of %.2f.\n", r, v);

  return 0;
}
  

  
