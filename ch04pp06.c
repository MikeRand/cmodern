// upc

#include <stdio.h>

int main(void) {
  int d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12,
    s1, s2, t;

  printf("Enter the first 12 digits of a EAN: ");
  scanf("%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d%1d",
	&d1, &d2, &d3, &d4, &d5, &d6,
	&d7, &d8, &d9, &d10, &d11, &d12);

  s1 = d2 + d4 + d6 + d8 + d10 + d12;
  s2 = d1 + d3 + d5 + d7 + d9 + d11;
  t = 3 * s1 + s2 - 1;
  t = 9 - (t % 10);
  
  printf("Check digit: %1d\n", t);

  return 0;

}
    

    
