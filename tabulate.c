#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void tabulate(double (*f)(double), double first,
							double last, double incr);

int main(void)
{
	double final, increment, initial;
	
	printf("Enter initial value: ");
	if (scanf("%lf", &initial) != 1)
		exit(EXIT_FAILURE);
	
	printf("Enter final value: ");
	if (scanf("%lf", &final) != 1)
		exit(EXIT_FAILURE);

	printf("Enter increment: ");
	if (scanf("%lf", &increment) != 1)
		exit(EXIT_FAILURE);

	printf("\n      x        cos(x)"
				 "\n   -------    -------\n");
	tabulate(cos, initial, final, increment);

	printf("\n      x        sin(x)"
				 "\n   -------    -------\n");
	tabulate(sin, initial, final, increment);

	printf("\n      x        tan(x)"
				 "\n   -------    -------\n");
	tabulate(tan, initial, final, increment);

	exit(EXIT_SUCCESS);
}

void tabulate(double (*f)(double), double first,
							double last, double incr)
{
	double x;
	int i, num_intervals;
	
	num_intervals = ceil((last - first) / incr);
	for (i = 0; i <= num_intervals; i++) {
		x = first + i * incr;
		printf("%10.5f %10.5f\n", x, (*f)(x));
	}
}
