// reverse digits

#include <stdio.h>

int main(void) {
  int number, first, second, third;

  printf("Enter a 3-digit number: ");
  scanf("%d", &number);
  
  first = number / 100;
  number %= 100;
  second = number / 10;
  third = number % 10;

  printf("The reversal is: %d%d%d\n", third, second, first);

  return 0;
}
