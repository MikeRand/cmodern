#include <stdio.h>
#include <stdlib.h>

#define N 5

int main(void) {
  int t;
  int a[N][N];

  for (int row = 0; row < N; row++) {
    printf("Enter row %d: ", row);
    for (int col = 0; col < N; col++) {
      scanf("%d", &a[row][col]);
    }
  }

  printf("Row totals:");
  for (int row = 0; row < N; row++) {
    t = 0;
    for (int col = 0; col < N; col++) {
      t += a[row][col];
    }
    printf(" %d", t);
  }
  printf("\n");

  printf("Column totals:");
  for (int col = 0; col < N; col++) {
    t = 0;
    for (int row = 0; row < N; row++) {
      t += a[row][col];
    }
    printf(" %d", t);
  }
  printf("\n");
  
  exit(EXIT_SUCCESS);
}

    
    
  
