// Convert phone number

#include <stdio.h>
#include <ctype.h>

int main(void) {
  int letter_value, word_value = 0;
  char letter;

  printf("Enter a word: ");
  
  while ((letter = getchar()) != '\n') {
    switch (toupper(letter)) {
      case 'Q': case 'Z':
	letter_value = 10;
	break;
      case 'J': case 'X': 
	letter_value = 8;
	break;
      case 'K':
	letter_value = 5;
	break;
      case 'F': case 'H': case 'V': case 'W': case 'Y':
	letter_value = 4;
	break;
      case 'B': case 'C': case 'M': case 'P':
	letter_value = 3;
	break;
      case 'D': case 'G':
	letter_value = 2;
	break;
      default:
	letter_value = 1;
	break;
    }
    word_value += letter_value;
  }

  printf("Scrabble value: %d\n", word_value);
  return 0;
}
