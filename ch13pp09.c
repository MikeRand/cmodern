// Count vowels

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_LEN 80

int read_line(char str[], int n);
int compute_vowel_count(const char *sentence);

int main(void) {

	char sentence[MAX_LEN + 1];

	printf("Enter a sentence: ");
	read_line(sentence, MAX_LEN);
	printf("Your sentence contains %d vowels.\n", 
				 compute_vowel_count(sentence));
	exit(EXIT_SUCCESS);
}

int compute_vowel_count(const char *sentence) {
	const char *chp;
	int vowel_count = 0;
	
	for (chp = sentence; *chp; chp++) {
		switch (toupper(*chp)) {
			case 'A': case 'E': case 'I': case 'O': case 'U':
				vowel_count++;
				break;
			default:
				break;
		}
	}
	return vowel_count;
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}

