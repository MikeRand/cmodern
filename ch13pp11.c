// Average word length.

#include <stdio.h>
#include <stdlib.h>

#define MAXLEN 200

int read_line(char str[], int n);
double compute_average_word_length(const char *sentence);

int main(void) {

	char sentence[MAXLEN + 1];
	printf("Enter a sentence: ");
	read_line(sentence, MAXLEN);
	printf("Average word length: %.1f\n", compute_average_word_length(sentence));

	exit(EXIT_SUCCESS);
}

double compute_average_word_length(const char *sentence) {

	const char *sptr;
	int words = 0, characters = 0;

	for (sptr = sentence; *sptr; sptr++) {
		switch (*sptr) {
			case ' ':
				words++;
				break;
			case '.': 
				words++;
				characters++; // dumb ... shouldn't count as a character.
				break;
			default:
				characters++;
				break;
		}
	}

	return (float) characters / words;
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
