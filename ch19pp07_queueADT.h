#ifndef QUEUE_H
#define QUEUE_H

#include <stdbool.h>

typedef struct queue_type *Queue;

typedef int Item;

Queue create(void);
void destroy(Queue q);
void make_empty(Queue q);
bool is_empty(Queue q);
bool is_full(Queue q);
void push(Queue q, Item i);
Item pop(Queue q);
Item first(Queue q);
Item last(Queue q);
int length(Queue q);

#endif
