// Calculate e

#include <stdio.h>

int main(void) {
  int n, denominator;
  float e;
  e = 1.00f;

  printf("Enter number of elements: ");
  scanf("%d", &n);

  for (int i = 1; i <= n; i++) {
    denominator = 1;
    for (int j = 1; j <= i; j++) {
      denominator *= j;
    }
    e += 1.00f / denominator;
  }

  printf("Value of e: %f\n", e);
}
    
