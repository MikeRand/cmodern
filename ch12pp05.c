// Reverse words in a sentence

#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 100

int main(void) {
	char sentence[MAX_LENGTH];
	char ch, terminal, *s, *start, *stop;

	terminal = '\0';
	
	printf("Enter a sentence: ");

	for (s = sentence; ((ch = getchar()) != '\n'); s++) {
		switch (ch) {
			case '.': case '!': case '?':
				terminal = ch;
				*s = '\0';
				break;
      default:
				*s = ch;
				break;
		}
	}

	printf("Check sentence: ");
	for (s = sentence; *s != '\0'; s++) {
		printf("%c", *s);
	}
	printf("%c\n", terminal);

	printf("Reversal of sentence:");

	for (start = s, stop = s; start >= sentence; start--) {
		if (*start == ' ' || start == sentence) {
			if (start == sentence)
				printf(" ");
			for (char *j = start; j < stop; j++) {
				printf("%c", *j);
			}
			stop = start;
		}
	}

	printf("%c\n", terminal);

	exit(EXIT_SUCCESS);
}
      
  

  
