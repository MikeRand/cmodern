// Scrabble

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
  int word_value = 0;
  int letter_values[] = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3,
			 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};
  char letter;

  printf("Enter a word: ");
  
  while ((letter = getchar()) != '\n') {
    word_value += letter_values[toupper(letter) - 'A'];
  }

  printf("Scrabble value: %d\n", word_value);
  return 0;
}
