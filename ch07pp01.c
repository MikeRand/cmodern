// short int: n = 181 = sqrt(2^15 - 1)
// int: n = 46341 = sqrt(2^31 - 1)
// long int (guess) n = 3037000499 = sqrt(2^63 - 1) (assuming n long)


#include <stdio.h>

int main(void) {
  int n;
  long int i;

  printf("Enter number of entries: ");
  scanf("%d", &n);
  
  for (i = 1; i <= n; i++) {
    printf("%10ld%10ld\n", i, i * i);
  }
  return 0;
}
