/* View float in 2 ways. */

#include <stdio.h>
#include <stdlib.h>

typedef union {
  float n;
  struct {
    unsigned int fraction: 23, exponent: 8, sign: 1;
  } fstruct;
} Float;

int main(void) {
  unsigned int sign, exponent, fraction;
  Float f;
  printf("Enter sign: ");
  if (scanf("%u", &sign) != 1)
    exit(EXIT_FAILURE);
  printf("Enter exponent: ");
  if (scanf("%u", &exponent) != 1)
    exit(EXIT_FAILURE);
  printf("Enter fraction: ");
  if (scanf("%u", &fraction) != 1)
    exit(EXIT_FAILURE);
  
  f.fstruct.sign = sign;
  f.fstruct.exponent = exponent;
  f.fstruct.fraction = fraction;

  printf("Float value is %f\n", f.n);
  
  printf("Enter float value: ");
  if (scanf("%f", &f.n) != 1)
    exit(EXIT_FAILURE);
    
  printf("Sign is %u\n", f.fstruct.sign);
  printf("Exponent is %u\n", f.fstruct.exponent);
  printf("Fraction is %u\n", f.fstruct.fraction);
  
  exit(EXIT_SUCCESS);
}
