#include <stdio.h>
#include <stdlib.h>

float polynomial(float x);

int main(void) {
  float x;
  printf("Enter x: ");
  scanf("%f", &x);
  printf("y: %f\n", polynomial(x));
  exit(EXIT_SUCCESS);
}

float polynomial(float x) {
  return ((((3 * x + 2) * x - 5) * x - 1) * x + 7) * x - 6;
}
  
