// Check palindrome

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define LEN 80

bool is_palindrome(char msg[], int n);

int main(void) {
	int i = 0;
	char msg[LEN];
	
	printf("Enter a message: ");
	while ((msg[i++] = getchar()) != '\n')
		;
	if (is_palindrome(msg, i))
		printf("Palindrome");
	else
		printf("Not a palindrome");
	printf("\n");
	
	exit(EXIT_SUCCESS);
}

bool is_palindrome(char msg[], int n) {
	int i, j;
	for (i = 0, j = n - 1; i <= j;) {
		if (isalpha(msg[i]) && isalpha(msg[j]) && toupper(msg[i]) != toupper(msg[j]))
			return false;
		else if (isalpha(msg[i]) && isalpha(msg[j])) {
			i++;
			j--;
		}
		else if (!isalpha(msg[i]))
			i++;
		if (!isalpha(msg[j]))
			j--;
	}
	return true;
}

