// Reverse words in a sentence

#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 100

int main(void) {
  int start, stop, i;
  char sentence[MAX_LENGTH];
  char ch, terminal;

  printf("Enter a sentence: ");
  
  for (i = 0; ((ch = getchar()) != '\n'); i++) {
    switch (ch) {
      case '.': case '!': case '?':
	terminal = ch;
	sentence[i] = '\0';
	break;
      default:
	sentence[i] = ch;
	break;
    }
  }

  printf("Check sentence: ");
  for (int j = 0; sentence[j] != '\0'; j++) {
    printf("%c", sentence[j]);
  }
  printf("%c\n", terminal);

  printf("Reversal of sentence:");
  
  for (start = i - 1, stop = i - 1; start >= 0; start--) {
    if (sentence[start] == ' ' || start == 0) {
      if (start == 0)
	printf(" ");
      for (int j = start; j < stop; j++) {
	printf("%c", sentence[j]);
      }
      stop = start;
    }
  }

  printf("%c\n", terminal);

  exit(EXIT_SUCCESS);
}
      
  

  
