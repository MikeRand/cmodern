// RPN calculator

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "stackADT.h"

int evaluate_expression(void);

int main(void) {
  int result;
  
  for (;;) {
    printf("Enter an RPN expression (q to quit): ");
    result = evaluate_expression();
    printf("Value of expression: %d\n", result);
  }
  exit(EXIT_FAILURE);
}

int evaluate_expression(void) {
  char ch;
  int result, op1, op2;
  Stack s = create();

  for (;;) {
    if(scanf(" %c", &ch) != 1)
			exit(EXIT_FAILURE);
    switch (ch) {
      case 'q': exit(EXIT_SUCCESS); break;
      case '=':
	result = pop(s);
	make_empty(s);
	return result;
	break;
      case '+':
	op2 = pop(s);
	op1 = pop(s);
	push(s, op1 + op2);
	break;
      case '-':
	op2 = pop(s);
	op1 = pop(s);
	push(s, op1 - op2);
	break;
      case '/':
	op2 = pop(s);
	op1 = pop(s);
	push(s, op1 / op2);
	break;
      case '*':
	op2 = pop(s);
	op1 = pop(s);
	push(s, op1 * op2);
	break;
      default:
        push(s, ch - '0'); // To convert character to integer.
	break;
    }
  }
}
