#include <stdio.h>
#include <stdlib.h>

#define NS 5
#define NQ 5

int main(void) {
  int tot, min, max;
  int grades[NS][NQ];

  for (int student = 0; student < NS; student++) {
    printf("Enter student quiz grades %d: ", student);
    for (int quiz = 0; quiz < NQ; quiz++) {
      scanf("%d", &grades[student][quiz]);
    }
  }

  printf("Student totals/avg:");
  for (int student = 0; student < NS; student++) {
    tot = 0;
    for (int quiz = 0; quiz < NQ; quiz++) {
      tot += grades[student][quiz];
    }
    printf(" %d/%.2f", tot, (float) tot / NQ);
  }
  printf("\n");

  printf("Quiz avg/min/max:");
  for (int quiz = 0; quiz < NQ; quiz++) {
    tot = 0;
    min = 10000;
    max = -10000;
    for (int student = 0; student < NS; student++) {
      tot += grades[student][quiz];
      if (grades[student][quiz] < min) {
	min = grades[student][quiz];
      }
      if (grades[student][quiz] > max) {
	max = grades[student][quiz];
      }
    }
    printf(" %.2f/%d/%d", (float) tot / NS, min, max);
  }
  printf("\n");
  
  exit(EXIT_SUCCESS);
}

    
    
  
