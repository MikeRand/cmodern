// upc

#include <stdio.h>

int main(void) {
  int d, i1, i2, i3, i4, i5, j1, j2, j3, j4, j5, s1, s2, t, c;

  printf("Enter the first (single) digit: ");
  scanf("%1d", &d);
  printf("Enter the first group of 5 digits: ");
  scanf("%1d%1d%1d%1d%1d", &i1, &i2, &i3, &i4, &i5);
  printf("Enter the second group of 5 digits: ");
  scanf("%1d%1d%1d%1d%1d", &j1, &j2, &j3, &j4, &j5);
  printf("Enter the check digit: ");
  scanf("%1d", &c);

  s1 = d + i2 + i4 + j1 + j3 + j5;
  s2 = i1 + i3 + i5 + j2 + j4;
  t = 3 * s1 + s2 - 1;
  t = 9 - (t % 10);
  
  printf("UPC %d%d%d%d%d%d%d%d%d%d%d%d is ", d, i1, i2, i3, i4, i5,
	 j1, j2, j3, j4, j5, c);
  if (c == t) {
    printf("VALID\n");
  } else {
    printf("NOT VALID (expected check digit of %d, received %d)\n",
	   t, c);
  }

  return 0;

}
    

    
