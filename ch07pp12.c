// Simple evaluator

#include <stdio.h>

int main(void) {
  float term, total;
  char operator;

  printf("Enter an expression: ");
  
  scanf("%f", &total);
  
  while ((operator = getchar()) != '\n') {
    scanf("%f", &term);
    switch (operator) {
      case '+':
	total += term;
	break;
      case '-':
	total -= term;
	break;
      case '*':
        total *= term;
        break;
      case '/':
        total *= term;
	break;
    }
  }
  printf("Value of expression: %.2f\n", total);
  return 0;
}
