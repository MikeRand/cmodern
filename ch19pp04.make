ch19pp04: ch19pp04.c ch19pp04_stackADT.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch19pp04 ch19pp04.c ch19pp04_stackADT.o

ch19pp04_stackADT.o: ch19pp04_stackADT.c ch19pp04_stackADT.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c ch19pp04_stackADT.c
