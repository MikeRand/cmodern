#include <stdio.h>
#include <stdlib.h>

int main(void) {
  
  int i, digit;
  int digits[10] = {0};
  long n;

  printf("Enter a number: ");
  scanf("%ld", &n);

  while (n > 0) {
    digit = n % 10;
    digits[digit]++;
    n /= 10;
  }

  printf("Digit:     ");
  for (i = 0; i < 10; i++) {
    printf("%3d", i);
  }
  printf("\n");

  printf("Occurences:");
  for (i = 0; i < 10; i++) {
    printf("%3d", digits[i]);
  }
  printf("\n");
    
  exit(EXIT_SUCCESS);
}
  
  
