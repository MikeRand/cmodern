// Magic square

#include <stdio.h>
#include <stdlib.h>

void create_magic_square(int n, char magic_square[n][n]);
void print_magic_square(int n, char magic_square[n][n]);

int main(void) {
  int n;

  printf("Enter size of magic square: ");
  scanf("%d", &n);

  char magic_square[n][n];
  create_magic_square(n, magic_square);
  print_magic_square(n, magic_square);
  exit(EXIT_SUCCESS);
}


void create_magic_square(int n, char magic_square[n][n]) {
  int i, row, col, next_row, next_col;

  for (int r = 0; r < n; r++) {
    for (int c = 0; c < n; c++) {
      magic_square[r][c] = 0;
    }
  }

  row = 0;
  col = n / 2;

  for (i = 1; i <= (n * n); i++) {
      magic_square[row][col] = i;
      if (row - 1 < 0) {
	next_row = n - 1;
      } else {
	next_row = row - 1;
      }
      if (col + 1 == n) {
	next_col = 0;
      } else {
	next_col = col + 1;
      }

      if (magic_square[next_row][next_col] == 0) {
	row = next_row;
	col = next_col;
      } else {
	if (row + 1 == n) {
	  row = 0;
	} else {
	  row = (row + 1);
	}
      }
  }
}


void print_magic_square(int n, char magic_square[n][n]) {
  for (int r = 0; r < n; r++) {
    for (int c = 0; c < n; c++) {
      printf("%5d", magic_square[r][c]);
    }
    printf("\n");
  }
}
