// sum.c

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int i, n = 0;
	
	for (i = 1; i < argc; i++)
		n += atoi(argv[i]);
		
	printf("Total: %d\n", n);
	exit(EXIT_SUCCESS);
}
