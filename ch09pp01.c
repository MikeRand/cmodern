#include <stdio.h>
#include <stdlib.h>

#define N 10

void selection_sort(int a[], int n);

int main(void) {
  
  int a[N];
  int n;

  printf("Enter %d numbers: ", N);
  for (n = 0; n < N; n++) {
    scanf("%d", &a[n]);
  }
  selection_sort(a, n);

  printf("Sorted:");
  for (n = 0; n < N; n++) {
    printf(" %d", a[n]);
  }
  printf("\n");

  exit(EXIT_SUCCESS);
}

void selection_sort(int a[], int n) {
  if (n > 1) {
    int tmp, max, maxidx;
    max = a[0];
    maxidx = 0;
    for (int i = 1; i < n; i++) {
      if (a[i] > max) {
	max = a[i];
	maxidx = i;
      }
    }
    tmp = a[n - 1];
    a[n - 1] = max;
    a[maxidx] = tmp;
    selection_sort(a, n - 1);
  }
}
  
  
