#include <stdio.h>
#include <stdlib.h>
#include "readline.h"

#define NAME_LEN 25

typedef struct Part {
	int number;
	char name[NAME_LEN+1];
	int on_hand;
	struct Part *next;
} Part, *PartPtr;

PartPtr inventory = NULL;

PartPtr find_part(int number);
void insert(void);
void erase(void);
void search(void);
void update(void);
void print(void);

int main(void) {
	char code;
	
	for (;;) {
		do {
			printf("Enter operation code: ");
		} while (scanf(" %c", &code) != 1);
		while (getchar() != '\n')
			;
		switch (code) {
			case 'i': insert(); break;
			case 'e': erase(); break;
			case 's': search(); break;
			case 'u': update(); break;
			case 'p': print(); break;
			case 'q': exit(EXIT_SUCCESS);
			default: printf("Illegal code\n");
		}
		printf("\n");
	}
}

PartPtr find_part(int number) {
	PartPtr p;
	
	for (p = inventory; p != NULL && number > p->number; p = p->next)
		;
		
	if (p != NULL && number == p->number)
		return p;
	return NULL;
}

void insert(void) {
	PartPtr cur, prev, new_node;
	
	new_node = malloc(sizeof(Part));
	if (new_node == NULL) {
		printf("Database is full; can't add more parts.\n");
		return;
	}
	
	do {
		printf("Enter part number: ");
	} while (scanf("%d", &new_node->number) != 1);
	
	for (cur = inventory, prev = NULL;
			 cur != NULL && new_node->number > cur->number;
			 prev = cur, cur = cur->next)
		;
	if (cur != NULL && new_node->number == cur->number) {
		printf("Part already exists.\n");
		free(new_node);
		return;
	}
	
	printf("Enter part name: ");
	read_line(new_node->name, NAME_LEN);
	do {
		printf("Enter quantity on hand: ");
	} while (scanf("%d", &new_node->on_hand) != 1);
	
	new_node->next = cur;
	if (prev == NULL)
		inventory = new_node;
	else
		prev->next = new_node;
}

void erase(void) {
	int number;
	PartPtr cur, prev;

	do {
		printf("Enter part number: ");
	} while (scanf("%d", &number) != 1);

	for (cur = inventory, prev = NULL;
			 cur != NULL && cur->number != number;
			 prev = cur, cur = cur->next)
		;
	if (cur == NULL) {
		printf("Part number not found.\n");
		return;
	}
	if (prev == NULL) {
		inventory = inventory->next;
		return;
	} else {
		prev->next = cur->next;
		free(cur);
		return;
	}
}

void search(void) {
	int number;
	PartPtr p;
	
	do {
		printf("Enter part number: ");
	} while (scanf("%d", &number) != 1);
	
	p = find_part(number);
	if (p != NULL) {
		printf("Part name: %s\n", p->name);
		printf("Quantity on hand: %d\n", p->on_hand);
	} else
		printf("Part not found.\n");
}

void update(void) {
	int number, change;
	PartPtr p;
	
	do {
		printf("Enter part number: ");
	} while (scanf("%d", &number) != 1);
	
	p = find_part(number);
	if (p != NULL) {
		do {
			printf("Enter change in quantity on hand: ");
		} while (scanf("%d", &change) != 1);
		p->on_hand += change;
	} else
		printf("Part not found.\n");
}

void print(void) {
	PartPtr p;
	
	printf("Part Number      Part Name                  "
				 "Quantity on Hand\n");
	for (p = inventory; p != NULL; p = p->next)
		printf("%7d          %-25s%11d\n", p->number, p->name, p->on_hand);
}
