// Last name first initial

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAXLEN 80

int read_line(char str[], int n);
void reverse_name(char *name);

int main(void) {
	char name[MAXLEN + 1];
  printf("Enter a first and last name: ");
	read_line(name, MAXLEN);
	reverse_name(name);
	printf("%s\n", name);
	exit(EXIT_SUCCESS);
}

void reverse_name(char *name) {
	char first_initial, *name_ptr, *name_ptr2;
	
  // Burn off initial whitespaces and find first initial.
  for (name_ptr = name; *name_ptr == ' '; name_ptr++) {
    ;
  }
  
  first_initial = *name_ptr++;

  // Burn off rest of first name.
  for (; *name_ptr != ' '; name_ptr++) {
    ;
  }

  // Burn off spaces
  for (; *name_ptr == ' '; name_ptr++) {
    ;
  }
  
  // Shift last name to front of array.
	for(name_ptr2 = name; *name_ptr; name_ptr++, name_ptr2++) {
		*name_ptr2 = *name_ptr;
	}

	// Add first initial.
	*(name_ptr2++) = ',';
	*(name_ptr2++) = ' ';
	*name_ptr2++ = first_initial;
	*name_ptr2++ = '.';
	*name_ptr2++ = '\0';
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
