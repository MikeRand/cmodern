// Reverse words in a sentence

#include <stdio.h>
#include <stdlib.h>

#define MAXWORDS 30
#define MAXLEN 20

int main(void) {

	int word = 0, letter = 0;
	char words[MAXWORDS][MAXLEN+1];
	char ch, terminal = '.';

	printf("Enter a sentence: ");

	while ((ch = getchar()) != '\n' && word < MAXWORDS && letter < MAXLEN) {
    switch (ch) {
      case '.': case '!': case '?':
				terminal = ch;
				words[word][letter] = '\0';
				break;
      case ' ':
				words[word++][letter] = '\0';
				letter = 0;
				break;
			default:
				words[word][letter++] = ch;
				break;
		}
	}

	printf("Reversal of sentence:");
	for (; word >= 0; word--) {
		printf(" %s", words[word]);
	}
	printf("%c\n", terminal);
	exit(EXIT_SUCCESS);
}
