// RPN calculator

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 100

char contents[STACK_SIZE];
int top = 0;

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int n);
int pop(void);
void stack_overflow(void);
void stack_underflow(void);
int evaluate_expression(void);
int evaluate_RPN_expression(const char *expression);
int read_line(char str[], int n);

int main(void) {
	
	char expression[STACK_SIZE + 1];
  
	for (;;) {
		printf("Enter an RPN expression (q to quit): ");
		read_line(expression, STACK_SIZE);
		printf("Value of expression: %d\n",
					 evaluate_RPN_expression(expression));
	}
	exit(EXIT_FAILURE);
}

int evaluate_RPN_expression(const char *expression) {
	const char *exp_ptr;
	int op1, op2, result = 0;

	for (exp_ptr = expression; *exp_ptr; exp_ptr++) {
		switch (*exp_ptr) {
			case 'q': case 'Q': exit(EXIT_SUCCESS); break;
			case '=':
				result = contents[top-1];
				make_empty();
				break;
			case '+':
				op2 = pop();
				op1 = pop();
				push(op1 + op2);
				break;
			case '-':
				op2 = pop();
				op1 = pop();
				push(op1 - op2);
				break;
			case '/':
				op2 = pop();
				op1 = pop();
				push(op1 / op2);
				break;
			case '*':
				op2 = pop();
				op1 = pop();
				push(op1 * op2);
				break;
			case ' ':
				break;
			default:
				push(*exp_ptr - '0'); // To convert character to integer.
				break;
		}
	}
	return result;
}

void make_empty(void) {
	top = 0;
}

bool is_empty(void) {
  return top == 0;
}

bool is_full(void) {
	return top == STACK_SIZE;
}

void push(int n) {
	if (is_full())
		stack_overflow();
	else
		contents[top++] = n;
}

int pop(void) {
  if (is_empty()) {
    stack_underflow();
    return 0;
  }
  else
    return contents[--top];
}

void stack_overflow(void) {
  printf("Expression is too complex.\n");
  exit(EXIT_SUCCESS);
}

void stack_underflow(void) {
  printf("Not enough operands in expression.\n");
  exit(EXIT_SUCCESS);
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}

