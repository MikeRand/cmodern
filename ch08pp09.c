#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main(void) {
  
  bool success = false;
  char ch = 'A';
  char grid[N][N];
  int direction;
  int row = 0, col = 0;
  int next_row, next_col;

  srand((unsigned) time(NULL));

  // Initialize loop.
  for (int r = 0; r < N; r++) {
    for (int c = 0; c < N; c++) {
      grid[r][c] = '.';
    }
  }

  // Start in top left
  grid[row][col] = ch++;

  for ( ; ch <= 'Z'; ch++) {

    success = false;
    direction = rand() % 4;

    for (int i = 0; i < 4; i++) {
      switch ((direction + i) % 4) {
        case 0:
	  next_row = row - 1;
	  next_col = col;
	  break;
        case 1:
	  next_row = row;
	  next_col = col + 1;
	  break;
        case 2:
	  next_row = row + 1;
	  next_col = col;
	  break;
        case 3:
	  next_row = row;
	  next_col = col - 1;
	  break;
      }
      if (0 <= next_row && next_row < N && 0 <= next_col &&
	  next_col < N && grid[next_row][next_col] == '.') {
	success = true;
	row = next_row;
	col = next_col;
	grid[row][col] = ch;
	break;
      }
    }
    if (!success) {
      break;
    }
  }

  // Print grid.
  for (int r = 0; r < N; r++) {
    for (int c = 0; c < N; c++) {
      printf(" %c", grid[r][c]);
    }
    printf("\n");
  }

  exit(EXIT_SUCCESS);
}
