#include <stdio.h>

int main(void) {
  int x, v;
  
  printf("Enter an integer x: ");
  scanf("%d", &x);

  v = 3 * x * x * x * x * x + 2 * x * x * x * x - 5 * x * x * x - x * x + 7 * x - 6;
  printf("Value of polynomial: %d\n", v);
  return 0;
}
    
