//Reverse message

#include <stdio.h>
#include <stdlib.h>

#define MSG_LEN 80

void reverse(char *message);
int read_line(char str[], int n);

int main(void) {
	
	char msg[MSG_LEN + 1];
	
	printf("Enter a message: ");
	read_line(msg, MSG_LEN);
	reverse(msg);
	printf("Reversal is: %s\n", msg);

	exit(EXIT_SUCCESS);
}

void reverse(char *message) {
	char tmp;
	char *start = message, *stop = message;
	
	while (*stop)
		stop++;
	
	for (stop--; start <= stop; start++, stop--) {
		tmp = *start;
		*start = *stop;
		*stop = tmp;
	}
}

int read_line(char str[], int n) {
	int ch, i = 0;
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
