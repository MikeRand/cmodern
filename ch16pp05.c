#include <stdio.h>
#include <stdlib.h>

typedef struct flight {
	int departure;
	int arrival;
} flight;

int main(void) {

	flight flights[] = {{8 * 60 + 0, 10 * 60 + 16},
											{9 * 60 + 43, 11 * 60 + 52},
											{11 * 60 + 19, 13 * 60 + 31},
											{12 * 60 + 47, 15 * 60 + 0},
											{14 * 60 + 0, 16 * 60 + 8},
											{15 * 60 + 45, 17 * 60 + 55},
											{19 * 60 + 0, 21 * 60 + 20},
											{21 * 60 + 45, 23 * 60 + 58}};

  int n, best_flightidx, best_dist, flightidx, abs_dist;
  int d, a, h, m, t, dh, dm, ah, am;

  printf("Enter a 24-hour time (hh:mm): ");
  if (scanf("%d:%d", &h, &m) != 2)
		exit(EXIT_FAILURE);
  t = h * 60 + m;
  
  n = sizeof(flights)/sizeof(flights[0]);
	
	best_flightidx = 0;
	best_dist = abs(flights[0].departure - t);
	
	for (flightidx = 1; flightidx < n; flightidx++) {
		abs_dist = abs(flights[flightidx].departure - t);
		if (abs_dist < best_dist) {
			best_flightidx = flightidx;
			best_dist = abs_dist;
		}
	}
	d = flights[best_flightidx].departure;
	a = flights[best_flightidx].arrival;

  if (d < 60) {
    dh = 12;
    dm = d;
  } else if (d < 13 * 60) {
    dh = d / 60;
    dm = d % 60;
  } else {
    dh = (d / 60) - 12;
    dm = d % 60;
  }  

  if (a < 60) {
    ah = 12;
    am = a;
  } else if (a < 12 * 60) {
    ah = a / 60;
    am = a % 60;
  } else {
    ah = (a / 60) - 12;
    am = a % 60;
  }

  printf("Closest departure time is %d:%.2d ", dh, dm);
  if (d < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf(", arriving at %d:%.2d ", ah, am);
  if (a < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf("\n");

  return 0;
}
  
