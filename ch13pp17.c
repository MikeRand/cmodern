// Check palindrome

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define LEN 80

int read_line(char str[], int n);
bool is_palindrome(const char *msg);

int main(void) {
	char msg[LEN + 1];
	
	printf("Enter a message: ");
	read_line(msg, LEN);
	
	if (is_palindrome(msg))
		printf("Palindrome");
	else
		printf("Not a palindrome");
	printf("\n");
	
	exit(EXIT_SUCCESS);
}

bool is_palindrome(const char *msg) {
	const char *i = msg, *j = msg;
	
	while (*j)
		j++;
	
	for (j--; i <= j;) {
		if (isalpha(*i) && isalpha(*j) && toupper(*i) != toupper(*j))
			return false;
		else if (isalpha(*i) && isalpha(*j)) {
			i++;
			j--;
		}
		else if (!isalpha(*i))
			i++;
		if (!isalpha(*j))
			j--;
	}
	return true;
}

int read_line(char str[], int n) {
	int ch, i = 0;
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
