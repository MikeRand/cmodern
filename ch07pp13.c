#include <stdio.h>

int main(void) {
  
  int words = 0, characters = 0;
  float word_length;
  char c;
  
  printf("Enter a sentence: ");

  while ((c = getchar()) != '\n') {
    switch (c) {
      case ' ':
        words++;
        break;
      case '.': 
	words++;
	characters++; // dumb ... shouldn't count as a character.
	break;
      default:
	characters++;
	break;
    }
  }
  
  word_length = (float) characters / words;
  printf("Average word length: %.1f\n", word_length);
  return 0;
}
  
    
  
