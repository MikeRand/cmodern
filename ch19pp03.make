ch19pp03: ch19pp03.c ch19pp03_stackADT.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch19pp03 ch19pp03.c ch19pp03_stackADT.o

ch19pp03_stackADT.o: ch19pp03_stackADT.c ch19pp03_stackADT.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c ch19pp03_stackADT.c
