// Sorts array using quicksort

#include <stdio.h>
#include <stdlib.h>
#include "quicksort.h"

#define N 10

int main(void) {
	int a[N], i;

	printf("Enter %d numbers to be sorted: ", N);
	for (i = 0; i < N; i++)
		if (scanf("%d", &a[i]) != 1)
			exit(EXIT_FAILURE);

	quicksort(a, 0, N - 1);

	printf("In sorted order: ");
	for (i = 0; i < N; i++)
		printf("%d ", a[i]);
	printf("\n");

	exit(EXIT_SUCCESS);
}
