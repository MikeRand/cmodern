// RPN calculator

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

extern int top;
extern char contents[]; // No size! declaring, not defining.

int main(void) {
	int result;
  
	for (;;) {
		printf("Enter an RPN expression (q to quit): ");
		result = evaluate_expression();
		printf("Value of expression: %d\n", result);
	}
	exit(EXIT_FAILURE);
}

int evaluate_expression(void) {
	char ch;
	int result, op1, op2;

	for (;;) {
		if (scanf(" %c", &ch) != 1)
			exit(EXIT_FAILURE);
		switch (ch) {
			case 'q': exit(EXIT_SUCCESS); break;
			case '=':
				result = contents[top-1];
				make_empty();
				return result;
				break;
			case '+':
				op2 = pop();
				op1 = pop();
				push(op1 + op2);
				break;
			case '-':
				op2 = pop();
				op1 = pop();
				push(op1 - op2);
				break;
			case '/':
				op2 = pop();
				op1 = pop();
				push(op1 / op2);
				break;
			case '*':
				op2 = pop();
				op1 = pop();
				push(op1 * op2);
				break;
			default:
				push(ch - '0'); // To convert character to integer.
				break;
		}
	}
}
