// Change date format.

#include <stdio.h>

int main(void) {
  int month, day, year;
  
  printf("Enter a data(mm/dd/yyyy): ");
  scanf("%d/%d/%d", &month, &day, &year);
  printf("You entered date %.4d%.2d%.2d\n", year, month, day);

  return 0;
}
