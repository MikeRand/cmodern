#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

void generate_random_walk(char walk[N][N]);
void print_array(char walk[N][N]);

int main(void) {
  
  char walk[N][N];

  generate_random_walk(walk);
  print_array(walk);

  exit(EXIT_SUCCESS);
}

void generate_random_walk(char walk[N][N]) {
  bool success = false;
  char ch = 'A';
  int direction;
  int row = 0, col = 0;
  int next_row, next_col;

  srand((unsigned) time(NULL));

  // Initialize loop.
  for (int r = 0; r < N; r++) {
    for (int c = 0; c < N; c++) {
      walk[r][c] = '.';
    }
  }

  // Start in top left
  walk[row][col] = ch++;

  for ( ; ch <= 'Z'; ch++) {

    success = false;
    direction = rand() % 4;

    for (int i = 0; i < 4; i++) {
      switch ((direction + i) % 4) {
        case 0:
	  next_row = row - 1;
	  next_col = col;
	  break;
        case 1:
	  next_row = row;
	  next_col = col + 1;
	  break;
        case 2:
	  next_row = row + 1;
	  next_col = col;
	  break;
        case 3:
	  next_row = row;
	  next_col = col - 1;
	  break;
      }
      if (0 <= next_row && next_row < N && 0 <= next_col &&
	  next_col < N && walk[next_row][next_col] == '.') {
	success = true;
	row = next_row;
	col = next_col;
	walk[row][col] = ch;
	break;
      }
    }
    if (!success) {
      break;
    }
  }
}

void print_array(char walk[N][N]) {
  for (int r = 0; r < N; r++) {
    for (int c = 0; c < N; c++) {
      printf(" %c", walk[r][c]);
    }
    printf("\n");
  }
}  
