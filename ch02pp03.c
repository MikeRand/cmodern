#include <stdio.h>
#include <math.h>

int main(void) {
  int r;
  float v;

  printf("Enter radius: ");
  scanf("%d", &r);

  v = 4.0f / 3.0f * M_PI * r * r * r;

  printf("A sphere of radius %d has a volume of %.2f.\n", r, v);

  return 0;
}
  

  
