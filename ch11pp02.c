#include <stdio.h>
#include <stdlib.h>

void find_closest_flight(int desired_time, int *departure_time,
												 int *arrival_time);


int main(void) {
	int h, m, t, d, a, dh, dm, ah, am;

	printf("Enter a 24-hour time (hh:mm): ");
	if (scanf("%d:%d", &h, &m) != 2)
		exit(EXIT_FAILURE);
	t = h * 60 + m;
	find_closest_flight(t, &d, &a);

  if (d < 60) {
    dh = 12;
    dm = d;
  } else if (d < 13 * 60) {
    dh = d / 60;
    dm = d % 60;
  } else {
    dh = (d / 60) - 12;
    dm = d % 60;
  }  

  if (a < 60) {
    ah = 12;
    am = a;
  } else if (a < 12 * 60) {
    ah = a / 60;
    am = a % 60;
  } else {
    ah = (a / 60) - 12;
    am = a % 60;
  }

  printf("Closest departure time is %d:%.2d ", dh, dm);
  if (d < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf(", arriving at %d:%.2d ", ah, am);
  if (a < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf("\n");

  exit(EXIT_SUCCESS);
}
  
void find_closest_flight(int desired_time, int *departure_time,
												 int *arrival_time) {
	
  int d1, d2, d3, d4, d5, d6, d7, d8, a1, a2, a3, a4, a5, a6, a7, a8;

	d1 = 8 * 60 + 0;
  a1 = 10 * 60 + 16;
  d2 = 9 * 60 + 43;
  a2 = 11 * 60 + 52;
  d3 = 11 * 60 + 19;
  a3 = 13 * 60 + 31;
  d4 = 12 * 60 + 47;
  a4 = 15 * 60 + 0;
  d5 = 14 * 60 + 0;
  a5 = 16 * 60 + 8;
  d6 = 15 * 60 + 45;
  a6 = 17 * 60 + 55;
  d7 = 19 * 60 + 0;
  a7 = 21 * 60 + 20;
  d8 = 21 * 60 + 45;
  a8 = 23 * 60 + 58;

  if (desired_time < ((d1 + d2) / 2)) {
    *departure_time =  d1;
    *arrival_time = a1;
  } else if (desired_time < ((d2 + d3) / 2)) {
    *departure_time =  d2;
    *arrival_time = a2;
  } else if (desired_time < ((d3 + d4) / 2)) {
    *departure_time =  d3;
    *arrival_time = a3;
  } else if (desired_time < ((d4 + d5) / 2)) {
    *departure_time =  d4;
    *arrival_time = a4;
  } else if (desired_time < ((d5 + d6) / 2)) {
    *departure_time =  d5;
    *arrival_time = a5;
  } else if (desired_time < ((d6 + d7) / 2)) {
    *departure_time =  d6;
    *arrival_time = a6;
  } else if (desired_time < ((d7 + d8) / 2)) {
    *departure_time =  d7;
    *arrival_time = a7;
  } else {
    *departure_time =  d8;
    *arrival_time = a8;
  }
}
