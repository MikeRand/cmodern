// Convert 12 to 24 hour time.

#include <stdio.h>
#include <ctype.h>

int main(void) {
  char aorp;
  int h, m, h12;

  h = 0;

  printf("Enter a 12-hour time (hh:mm AM/PM): ");
  scanf(" %d:%d %c", &h12, &m, &aorp);
  if (toupper(aorp) == 'A') {
    if (h12 == 12) {
      h = 0;
    } else {
      h = h12;
    }
  } else if (toupper(aorp) == 'P') {
    if (h12 == 12) {
      h = h12;
    } else {
      h = h12 + 12;
    }
  }

  printf("Equivalent 24-hour time: %d:%.2d\n", h, m);

  return 0;
}
  
