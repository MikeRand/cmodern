#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readline.h"

#define MAX_WORDS 60
#define WORD_LEN 20

int compare_words(const void *p, const void *q);

int main(void) {
	char *words[MAX_WORDS];
	char word[WORD_LEN+1];
	int num_words = 0;
	
	for (;;) {
		
		if (num_words == MAX_WORDS) {
			printf("-- No space left --\n");
			break;
		}
		
		printf("Enter word: ");

		read_line(word, WORD_LEN);

		if (strlen(word) == 0)
			break;

		words[num_words] = malloc(strlen(word) + 1);
		if (words[num_words] == NULL) {
			printf("-- No space left --\n");
			break;
		}
		strcpy(words[num_words], word);
		num_words++;
	}

	qsort(words, num_words, sizeof(*words), compare_words); // size of ptr
	printf("In sorted order:");
	for (int i = 0; i < num_words; i++)
		printf(" %s", words[i]);
	printf("\n");
	exit(EXIT_SUCCESS);
}

int compare_words(const void *p, const void *q) {
	return strcmp(*(const char **)p, *(const char **)q);
}
