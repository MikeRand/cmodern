// Number to letters.

#include <stdio.h>
#include <stdlib.h>

int main(void) {
  int number, digit1, digit2;
  
  char *tens[] = {"ones",
									"tens",
									"twenty",
									"thirty",
									"forty",
									"fifty",
									"sixty",
									"seventy",
									"eighty",
									"ninety"};
									
	char *teens[] = {"ten",
									 "eleven",
									 "twelve",
									 "thirteen",
									 "fourteen",
									 "fifteen",
									 "sixteen",
									 "seventeen",
									 "eighteen",
									 "nineteen"};
									 
	char *ones[] = {"zero",
									"one",
									"two",
									"three",
									"four",
									"five",
									"six",
									"seven",
									"eight",
									"nine"};
  
  printf("Enter a two-digit number: ");
  if (scanf("%d", &number) != 1)
		exit(EXIT_FAILURE);
  
  digit1 = number / 10;
  digit2 = number % 10;

  printf("You entered the number ");
	if (digit1 >= 2 && digit2 == 0)
		printf("%s", tens[digit1]);
	else if (digit1 >= 2)
		printf("%s-%s", tens[digit1], ones[digit2]);
	else if (digit1 == 1)
		printf("%s", teens[digit2]);
	else if (digit1 == 0)
		printf("%s", ones[digit2]);
	else
		exit(EXIT_FAILURE);
  
  printf(".\n");
  return 0;
}
