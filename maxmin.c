#include <stdio.h>
#include <stdlib.h>

#define N 10

void max_min(int a[], int n, int *max, int *min);

int main(void) {
	
	int max, min;
	int a[N];
	
	printf("Enter 10 numbers: ");
	for (int i = 0; i < N; i++) {
		if (scanf(" %d", &a[i]) != 1)
			exit(EXIT_FAILURE);
	}
	max_min(a, N, &max, &min);
	printf("Largest: %d\n", max);
	printf("Smallest: %d\n", min);
	
	exit(EXIT_SUCCESS);
}

void max_min(int a[], int n, int *max, int *min) {
	*max = *min = a[0];
	for (int i = 1; i < n; i++) {
		if (a[i] > *max)
			*max = a[i];
		else if (a[i] < *min)
			*min = a[i];
	}
}
