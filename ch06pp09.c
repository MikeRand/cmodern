#include <stdio.h>

int main(void) {
  int n;
  float balance, rate, pmt, mrate;
  
  printf("Enter amount of loan: ");
  scanf("%f", &balance);
  printf("Enter interest rate: ");
  scanf("%f", &rate);
  printf("Enter monthly payment: ");
  scanf("%f", &pmt);
  printf("Enter number of payments: ");
  scanf("%d", &n);

  mrate = rate / 1200.f;

  for (int i = 1; i <= n; i++) {
    balance = balance * (1 + mrate) - pmt;
    printf("Balance remaing after payment %3d: $%12.2f\n", i, balance);
  }
  return 0;
}
