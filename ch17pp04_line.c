#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "line.h"

#define MAX_LINE_LEN 60

typedef struct Word {
	char *word;
	struct Word *next;
} Word, *WordPtr;

WordPtr first = NULL;
int line_len = 0;
int num_words = 0;

void debug_line(const char *method) {
	printf("***debug_line(%s)", method);
	if (first == NULL)
		printf(" first=NULL");
	else
		printf(" first=%s", first->word);
	printf(" line_len=%d", line_len);
	printf(" num_words=%d", num_words);
	printf("***\n");
}

void clear_line(void) {
	WordPtr tmp;

	while (first != NULL) {
		tmp = first;
		first = first->next;
		free(tmp);
	}
	first = NULL;
	line_len = 0;
	num_words = 0;

}

void add_word(const char *word) {

	WordPtr new_word, prev, cur;
	
	new_word = malloc(sizeof(Word));
	if (new_word == NULL) {
		printf("malloc failed in add_word\n");
		return;
	}
	new_word->word = malloc(strlen(word) + 1);
	if (new_word->word == NULL) {
		printf("malloc failed in add_word\n");
		free(new_word);
		return;
	}
	strcpy(new_word->word, word);
	new_word->next = NULL;  //It will be at the end of the linked list.

	line_len += strlen(word);
	if (num_words > 0)  // To add a space before the word.
		line_len++;

	for (cur = first, prev = NULL;
			 cur != NULL;
			 prev = cur, cur = cur->next)
		;

	if (prev == NULL)  // First word
		first = new_word;
	else
		prev->next = new_word;

	num_words++;

}

int space_remaining(void) {
	return MAX_LINE_LEN - line_len;
}

void write_line(void) {
	int extra_spaces, spaces_to_insert, i;
	WordPtr cur;

	extra_spaces = space_remaining();
	
	// Loop through words in linked list
	for (cur = first; cur != NULL; cur = cur->next) {
		printf("%s", cur->word);
		if (num_words > 1) {
			spaces_to_insert = extra_spaces / (num_words - 1);
			for (i = 1; i <= spaces_to_insert + 1; i++)
				putchar(' ');
			extra_spaces -= spaces_to_insert;
			num_words--;
		}
	}
	putchar('\n');
}

void flush_line(void) {
	WordPtr cur;
	
	if (line_len > 0) {
		for (cur = first; cur != NULL; cur = cur->next) {
			printf("%s", cur->word);
			if (num_words > 0)
				putchar(' ');
			num_words--;
		}
		putchar('\n');
	}
}
