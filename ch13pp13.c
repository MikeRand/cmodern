// Caeser cipher

#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 80

void encrypt(char *message, int shift);
int read_line(char str[], int n);

int main(void) {

	char message[MAX_LENGTH + 1];
	int shift;

	printf("Enter message to be encrypted: ");
	read_line(message, MAX_LENGTH);
	printf("Enter shift amount (0-25): ");
	if (scanf("%d", &shift) != 1)
		exit(EXIT_FAILURE);

	encrypt(message, shift);
	printf("Encrypted message: %s\n", message);

	exit(EXIT_SUCCESS);
}

void encrypt(char *message, int shift) {
	char *msg_ptr;
	
	for (msg_ptr = message; *msg_ptr; msg_ptr++) {
		if (*msg_ptr >= 'A' && *msg_ptr <= 'Z')
			*msg_ptr = ((*msg_ptr - 'A') + shift) % 26 + 'A';
		else if (*msg_ptr >= 'a' && *msg_ptr <= 'z')
			*msg_ptr = ((*msg_ptr - 'a') + shift) % 26 + 'a';
	}
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
