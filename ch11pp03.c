// Fraction reduction

#include <stdio.h>
#include <stdlib.h>

void reduce(int numerator, int denominator,
						int *reduced_numerator, int *reduced_denominator);

int main(void) {
  
  int n, d, rn, rd;
  
  printf("Enter a fraction: ");
  if (scanf("%d/%d", &n, &d) != 2)
		exit(EXIT_FAILURE);
	reduce(n, d, &rn, &rd);
  printf("In lowest terms: %d/%d\n", rn, rd);
  exit(EXIT_SUCCESS);
}
    
void reduce(int numerator, int denominator,
						int *reduced_numerator, int *reduced_denominator) {
	
	int m, n, remainder;
	m = numerator;
	n = denominator;
	
	while (n != 0) {
    remainder = m % n;
    m = n;
    n = remainder;
  }
  *reduced_numerator = numerator / m;
  *reduced_denominator = denominator / m;
}
