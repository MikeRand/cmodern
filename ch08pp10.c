#include <stdio.h>
#include <stdlib.h>

int main(void) {
  const int departures[8] = {8*60, 9*60+43, 11*60+19, 12*60+47, 14*60,
		       15*60+45, 19*60, 21*60+45};
  const int arrivals[8] = {10*60+16, 11*60+52, 13*60+31, 15*60,
			   16*60+8, 17*60+55, 21*60+20, 23*60+58};


  int d, a, h, m, t, dh, dm, ah, am;

  printf("Enter a 24-hour time (hh:mm): ");
  scanf("%d:%d", &h, &m);
  t = h * 60 + m;

  if (t > ((departures[6] + departures[7]) / 2)) {
    d = departures[7];
    a = arrivals[7];
  } else {
    d = departures[0];
    a = arrivals[0];
    for (int i = 0; i < 7; i++) {
      if (t < ((departures[i] + departures[i+1]) / 2)) {
	d = departures[i];
	a = arrivals[i];
	break;
      }
    }
  }

  if (d < 60) {
    dh = 12;
    dm = d;
  } else if (d < 13 * 60) {
    dh = d / 60;
    dm = d % 60;
  } else {
    dh = (d / 60) - 12;
    dm = d % 60;
  }  

  if (a < 60) {
    ah = 12;
    am = a;
  } else if (a < 12 * 60) {
    ah = a / 60;
    am = a % 60;
  } else {
    ah = (a / 60) - 12;
    am = a % 60;
  }

  printf("Closest departure time is %d:%.2d ", dh, dm);
  if (d < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf(", arriving at %d:%.2d ", ah, am);
  if (a < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf("\n");

  return 0;
}
  
