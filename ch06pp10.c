// Least of n dates

#include <stdio.h>

int main(void) {
  int m, d, y, day, dayl, yl, ml, dl;
  
  printf("Enter date (mm/dd/yy): ");
  scanf("%d/%d/%d", &m, &d, &y);
  day = y * 360 + m * 30 + d;
  dayl = day;

  while (day != 0) {
    printf("Enter date (mm/dd/yy): ");
    scanf("%d/%d/%d", &m, &d, &y);
    day = y * 360 + m * 30 + d;
    if (day == 0) {
	break;
    }
    if (day < dayl) {
      dayl = day;
    }
  }
  if (dayl != 0) {
    yl = dayl / 360;
    ml = (dayl % 360) / 30;
    dl = (dayl % 360) % 30;
    printf("Lowest date: %d/%d/%.2d\n", ml, dl, yl);
  }
  return 0;
}
  
