// Decimal to octal

#include <stdio.h>

int main(void) {
  int number, first, second, third, forth, fifth;

  printf("Enter a number between 0 and 32767: ");
  scanf("%d", &number);

  fifth = number % 8;
  number /= 8;
  forth = number % 8;
  number /= 8;
  third = number % 8;
  number /= 8;
  second = number % 8;
  number /= 8;
  first = number % 8;
  printf("In octal, your number is: %1d%1d%1d%1d%1d\n",
	 first, second, third, forth, fifth);

  return 0;
}
