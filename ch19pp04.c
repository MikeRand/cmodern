#include <stdio.h>
#include <stdlib.h>
#include "ch19pp04_stackADT.h"

int main(void)
{
	Stack s1, s2;
	char *s;
	
	s1 = create();
	s2 = create();

	push(s1, "First string");
	push(s1, "Second string");
	
	s = pop(s1);
	printf("Popped %s from s1\n", s);
	push(s2, s);
	s = pop(s1);
	printf("Popped %s from s1\n", s);
	push(s2, s);
	
	destroy(s1);
	
	while (!is_empty(s2))
		printf("Popped %s from s2\n", (char *) pop(s2));
		
	push(s2, "Third string");
	make_empty(s2);
	if (is_empty(s2))
		printf("s2 is empty\n");
	else
		printf("s2 is not empty\n");
		
	destroy(s2);
	
	exit(EXIT_SUCCESS);
}
