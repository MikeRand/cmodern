// upc

#include <stdio.h>

int main(void) {
  int upc, d, i1, i2, i3, i4, i5, j1, j2, j3, j4, j5, s1, s2, t;

  printf("Enter the first 11 digits of a UPC: ");
  scanf("%d", &upc);
  j5 = upc % 10;
  upc /= 10;
  j4 = upc % 10;
  upc /= 10;
  j3 = upc % 10;
  upc /= 10;
  j2 = upc % 10;
  upc /= 10;
  j1 = upc % 10;
  upc /= 10;
  i5 = upc % 10;
  upc /= 10;
  i4 = upc % 10;
  upc /= 10;
  i3 = upc % 10;
  upc /= 10;
  i2 = upc % 10;
  upc /= 10;
  i1 = upc % 10;
  upc /= 10;
  d = upc % 10;

  s1 = d + i2 + i4 + j1 + j3 + j5;
  s2 = i1 + i3 + i5 + j2 + j4;
  t = 3 * s1 + s2 - 1;
  t = 9 - (t % 10);
  
  printf("Check digit: %1d\n", t);

  return 0;

}
    

    
