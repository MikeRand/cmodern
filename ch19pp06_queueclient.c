#include <stdio.h>
#include <stdlib.h>
#include "ch19pp06_queueADT.h"

int main(void) {
	Queue q1, q2;
	int n;
	
	q1 = create(100);
	q2 = create(100);
	
	for (int i = 0; i < 10; i++) {
		push(q1, i);
		printf("push %d into q1 ... length=%d, first=%d, last=%d\n",
					 i, length(q1), first(q1), last(q1));
	}

	while (!is_empty(q1)) {
		n = pop(q1);
		push(q2, n);
		printf("pop %d from q1 and push into q2 ... length1=%d, length2=%d,"
					 " first=%d, last=%d\n",
					 n, length(q1), length(q2), first(q2), last(q2));
	}
	
	make_empty(q2);
	
	if (is_empty(q2))
		printf("q2 is empty\n");
	else {
		printf("q2 is not empty\n");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
