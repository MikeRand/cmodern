#include <stdio.h>

/************************************************************
 * Limits (first failure)
 * short:   8
 * int:    17
 * long:   21
 ************************************************************/


int main(void) {
  int x;
  long long fact = 1LL;
  
  printf("Enter a positive integer: ");
  scanf("%d", &x);
  
  for (long long i = x; i > 0; i--) {
    fact *= i;
  }

  printf("Factorial of %d: %lld\n", x, fact);
  return 0;
}
  
