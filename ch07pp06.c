// Print size of types

#include <stdio.h>

#define BITS 8

int main(void) {

  printf("int: %zu\n", sizeof(int) * BITS);
  printf("short: %zu\n", sizeof(short) * BITS);
  printf("long: %zu\n", sizeof(long) * BITS);
  printf("float: %zu\n", sizeof(float) * BITS);
  printf("double: %zu\n", sizeof(double) * BITS);
  printf("long double: %zu\n", sizeof(long double) * BITS);
  return 0;
}
