// reverse.c

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	char **p = argv;
	for (p = argv + argc - 1; p > argv; p--)
		printf("%s ", *p);
	printf("\n");
	exit(EXIT_SUCCESS);
}
