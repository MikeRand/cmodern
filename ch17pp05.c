#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readline.h"

#define MAX_WORDS 60
#define WORD_LEN 20

int main(void) {
	char *words[MAX_WORDS];
	char word[WORD_LEN+1];
	int i, j, num_words = 0;
	
	for (;;) {
		
		if (num_words == MAX_WORDS) {
			printf("-- No space left --\n");
			break;
		}
		
		printf("Enter word: ");

		read_line(word, WORD_LEN);

		if (strlen(word) == 0)
			break;

		for (i = 0; i < num_words; i++)
			if (strcmp(word, words[i]) < 0)
				break;
		for (j = num_words; j > i; j--)
			words[j] = words[j-1];
			
		words[i] = malloc(strlen(word) + 1);
		if (words[i] == NULL) {
			printf("-- No space left --\n");
			break;
		}
		
		strcpy(words[i], word);
		
		num_words++;
	}
	
	printf("In sorted order:");
	for (i = 0; i < num_words; i++)
		printf(" %s", words[i]);
	printf("\n");
	exit(EXIT_SUCCESS);
}
