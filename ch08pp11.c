// Convert phone number

#include <stdio.h>
#include <stdlib.h>

#define N 15

int main(void) {
  
  int i;
  char in_char, out_char;
  char phone_number[N] = {' '};

  printf("Enter phone number: ");
  
  for (i = 0; (in_char = getchar()) != '\n'; i++) {
    switch (in_char) {
      case 'A': case 'B': case 'C':
	out_char = '2';
	break;
      case 'D': case 'E': case 'F':
	out_char = '3';
	break;
      case 'G': case 'H': case 'I':
	out_char = '4';
	break;
      case 'J': case 'K': case 'L':
	out_char = '5';
	break;
      case 'M': case 'N': case 'O':
	out_char = '6';
	break;
      case 'P': case 'R': case 'S':
	out_char = '7';
	break;
      case 'T': case 'U': case 'V':
	out_char = '8';
	break;
      case 'W': case 'X': case 'Y':
	out_char = '9';
	break;
      default:
	out_char = in_char;
	break;
    }
    phone_number[i] = out_char;
  }
  phone_number[i] = '\0';

  printf("In numeric form: ");
  for (int i = 0; phone_number[i] != '\0'; i++) {
    printf("%c", phone_number[i]);
  }
  printf("\n");
  exit(EXIT_SUCCESS);
}
