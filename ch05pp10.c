// Letter grade

#include <stdio.h>

int main(void) {
  int g, g1;

  printf("Enter numerical grade: ");
  scanf("%d", &g);

  if ((g > 100) || (g < 0)) {
    printf("Error: %d outside of acceptable range (0-100).\n", g);
    return 0;
  }

  g1 = g / 10;
  printf("Letter grade: ");
  switch (g1) {
    case 10:
    case 9:
      printf("A");
    case 8:
      printf("B");
      break;
    case 7:
      printf("C");
      break;
    case 6:
      printf("D");
      break;
    default:
      printf("F");
      break;
  }

  printf("\n");

  return 0;
}
  
  
      
