// RPN calculator

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define STACK_SIZE 100

char contents[STACK_SIZE];
int top = 0;

void make_empty(void);
bool is_empty(void);
bool is_full(void);
void push(int n);
int pop(void);
void stack_overflow(void);
void stack_underflow(void);
int evaluate_expression(void);

int main(void) {
  int result;
  
  for (;;) {
    printf("Enter an RPN expression (q to quit): ");
    result = evaluate_expression();
    printf("Value of expression: %d\n", result);
  }
  exit(EXIT_FAILURE);
}

int evaluate_expression(void) {
  char ch;
  int result, op1, op2;

  for (;;) {
    scanf(" %c", &ch);
    switch (ch) {
      case 'q': exit(EXIT_SUCCESS); break;
      case '=':
	result = contents[top-1];
	make_empty();
	return result;
	break;
      case '+':
	op2 = pop();
	op1 = pop();
	push(op1 + op2);
	break;
      case '-':
	op2 = pop();
	op1 = pop();
	push(op1 - op2);
	break;
      case '/':
	op2 = pop();
	op1 = pop();
	push(op1 / op2);
	break;
      case '*':
	op2 = pop();
	op1 = pop();
	push(op1 * op2);
	break;
      default:
        push(ch - '0'); // To convert character to integer.
	break;
    }
  }
} 

void make_empty(void) {
  top = 0;
}

bool is_empty(void) {
  return top == 0;
}

bool is_full(void) {
  return top == STACK_SIZE;
}

void push(int n) {
  if (is_full())
    stack_overflow();
  else
    contents[top++] = n;
}

int pop(void) {
  if (is_empty()) {
    stack_underflow();
    return 0;
  }
  else
    return contents[--top];
}

void stack_overflow(void) {
  printf("Expression is too complex.\n");
  exit(EXIT_SUCCESS);
}

void stack_underflow(void) {
  printf("Not enough operands in expression.\n");
  exit(EXIT_SUCCESS);
}
