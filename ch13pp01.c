#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXWORD 20

int read_line(char str[]);

int main(void) {
	int n;
	char largest_word[MAXWORD+1], smallest_word[MAXWORD+1], word[MAXWORD+1];
	
	printf("Enter word: ");
	n = read_line(word);
	strcpy(smallest_word, word);
	strcpy(largest_word, word);
	
	for(; n != 4;) {
		printf("Enter word: ");
		n = read_line(word);
		if (strcmp(word, smallest_word) <= 0)
			strcpy(smallest_word, word);
		else if (strcmp(word, largest_word) >= 0)
			strcpy(largest_word, word);
	}
	
	printf("Smallest word: %s\n", smallest_word);
	printf("Largest word: %s\n", largest_word);

	exit(EXIT_SUCCESS);
}

int read_line(char str[]) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < MAXWORD)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
