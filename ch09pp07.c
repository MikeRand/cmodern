#include <stdio.h>
#include <stdlib.h>

long power(int x, int n);

int main(void) {
  int x, n;
  printf("Enter x: ");
  scanf("%d", &x);
  printf("Enter n: ");
  scanf("%d", &n);
  printf("x ** n: %ld\n", power(x, n));
  exit(EXIT_SUCCESS);
}

long power(int x, int n) {
  long result;
  if (n == 0) {
    return 1;
  } else if (n % 2 == 0) {
    result = power(x, n / 2);
    return result * result;
  } else {
    return x * power(x, n - 1);
  }
}
    
  
  
