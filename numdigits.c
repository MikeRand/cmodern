// Count number of digits in an integer

#include <stdio.h>

int main(void) {
  int n, digits = 0;
  printf("Enter a non-negative number: ");
  scanf("%d", &n);

  do {
    n /= 10;
    digits += 1;
  } while (n > 0);

  printf("Digits: %d\n", digits);
  return 0;
}
