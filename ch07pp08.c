#include <stdio.h>
#include <ctype.h>

int main(void) {
  char aorp;
  int d1, d2, d3, d4, d5, d6, d7, d8, a1, a2, a3, a4, a5, a6, a7, a8,
    d, a, h, m, t, dh, dm, ah, am, h12;

  d1 = 8 * 60 + 0;
  a1 = 10 * 60 + 16;
  d2 = 9 * 60 + 43;
  a2 = 11 * 60 + 52;
  d3 = 11 * 60 + 19;
  a3 = 13 * 60 + 31;
  d4 = 12 * 60 + 47;
  a4 = 15 * 60 + 0;
  d5 = 14 * 60 + 0;
  a5 = 16 * 60 + 8;
  d6 = 15 * 60 + 45;
  a6 = 17 * 60 + 55;
  d7 = 19 * 60 + 0;
  a7 = 21 * 60 + 20;
  d8 = 21 * 60 + 45;
  a8 = 23 * 60 + 58;

  h = 0;

  printf("Enter a 12-hour time (hh:mm AM/PM): ");
  scanf(" %d:%d %c", &h12, &m, &aorp);
  if (toupper(aorp) == 'A') {
    if (h12 == 12) {
      h = 0;
    } else {
      h = h12;
    }
  } else if (toupper(aorp) == 'P') {
    if (h12 == 12) {
      h = h12;
    } else {
      h = h12 + 12;
    }
  }

  t = h * 60 + m;

  if (t < ((d1 + d2) / 2)) {
    d = d1;
    a = a1;
  } else if (t < ((d2 + d3) / 2)) {
    d = d2;
    a = a2;
  } else if (t < ((d3 + d4) / 2)) {
    d = d3;
    a = a3;
  } else if (t < ((d4 + d5) / 2)) {
    d = d4;
    a = a4;
  } else if (t < ((d5 + d6) / 2)) {
    d = d5;
    a = a5;
  } else if (t < ((d6 + d7) / 2)) {
    d = d6;
    a = a6;
  } else if (t < ((d7 + d8) / 2)) {
    d = d7;
    a = a7;
  } else {
    d = d8;
    a = a8;
  }

  if (d < 60) {
    dh = 12;
    dm = d;
  } else if (d < 13 * 60) {
    dh = d / 60;
    dm = d % 60;
  } else {
    dh = (d / 60) - 12;
    dm = d % 60;
  }  

  if (a < 60) {
    ah = 12;
    am = a;
  } else if (a < 12 * 60) {
    ah = a / 60;
    am = a % 60;
  } else {
    ah = (a / 60) - 12;
    am = a % 60;
  }

  printf("Closest departure time is %d:%.2d ", dh, dm);
  if (d < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf(", arriving at %d:%.2d ", ah, am);
  if (a < (12 * 60)) {
    printf("a.m.");
  } else {
    printf("p.m.");
  }
  printf("\n");

  return 0;
}
  
