#include <stdio.h>

int main(void) {
  
  int amt, remain, twenties, tens, fives, ones;
  
  printf("Enter a dollar amount: ");
  scanf("%d", &amt);

  remain = amt;
  twenties = remain / 20;
  remain -= twenties * 20;
  tens = remain / 10;
  remain -= tens * 10;
  fives = remain / 5;
  remain -= fives * 5;
  ones = remain / 1;
  remain -= ones;

  printf("$20 bills: %d\n", twenties);
  printf("$10 bills: %d\n", tens);
  printf(" $5 bills: %d\n", fives);
  printf(" $1 bills: %d\n", ones);
  
  return 0;

}
