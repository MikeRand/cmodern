// Reformat telephone number

#include <stdio.h>

int main(void) {
  int area_code, first, second;
  
  printf("Enter telephone number [(xxx) xxx-xxxx]: ");
  scanf("(%d) %d-%d", &area_code, &first, &second);
  printf("You entered %d.%d.%d\n", area_code, first, second);

  return 0;

}
