// Count vowels

#include <stdio.h>
#include <ctype.h>

int main(void) {
  char c;
  int vowel_count = 0;

  printf("Enter a sentence: ");

  while ((c = getchar()) != '\n') {
    switch (toupper(c)) {
      case 'A': case 'E': case 'I': case 'O': case 'U':
        vowel_count++;
	break;
    }
  }

  printf("Your sentence contains %d vowels.\n", vowel_count);
  return 0;
}
