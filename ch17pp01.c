#include <stdio.h>
#include <stdlib.h>
#include "readline.h"

#define NAME_LEN 25
#define INITIAL_PARTS 10

typedef struct Part {
	int number;
	char name[NAME_LEN+1];
	int on_hand;
} Part, *PartPtr;

PartPtr inventory;
int num_parts = 0;
int max_parts = INITIAL_PARTS;

int find_part(int number);
void insert(void);
void search(void);
void update(void);
void print(void);

int main(void) {
	char code;
	
	inventory = malloc(max_parts * sizeof(Part));
	if (inventory == NULL) {
		printf("malloc failed in main. Exiting...\n");
		exit(EXIT_FAILURE);
	}
	
	for (;;) {
		printf("Enter operation code: ");
		if (scanf(" %c", &code) != 1)
			exit(EXIT_FAILURE);
		while (getchar() != '\n')
			;
		switch (code) {
			case 'i': insert(); break;
			case 's': search(); break;
			case 'u': update(); break;
			case 'p': print(); break;
			case 'q': free(inventory); inventory = NULL; exit(EXIT_SUCCESS);
			default: printf("Illegal code ... i/s/u/p/q only.\n");
		}
	printf("\n");
	}
}

int find_part(int number) {
	int i;
	for (i = 0; i < num_parts; i++)
		if (inventory[i].number == number)
			return i;
	return -1;
}

void insert(void) {
	int part_number;
	PartPtr temp;
	
	if (num_parts == max_parts) {
		temp = realloc(inventory, max_parts * 2 * sizeof(Part));
		if (temp == NULL) {
			printf("No more space; can't add more parts.\n");
			return;
		}
		inventory = temp;
		max_parts *= 2;
	}
	printf("Enter Part number: ");
	if (scanf("%d", &part_number) != 1)
		exit(EXIT_FAILURE);
	if (find_part(part_number) >= 0) {
		printf("Part already exists.\n");
		return;
	}
	inventory[num_parts].number = part_number;
	printf("Enter Part name: ");
	read_line(inventory[num_parts].name, NAME_LEN);
	printf("Enter quantity on hand: ");
	if (scanf("%d", &inventory[num_parts].on_hand) != 1)
		exit(EXIT_FAILURE);
	num_parts++;
}

void search(void) {
	int i, number;
	printf("Enter Part number: ");
	if (scanf("%d", &number) != 1)
		exit(EXIT_FAILURE);
	i = find_part(number);
	if (i >= 0) {
		printf("Part name: %s\n", inventory[i].name);
		printf("Quantity on hand: %d\n", inventory[i].on_hand);
	} else
		printf("Part not found.\n");
}

void update(void) {
	int i, number, change;
	
	printf("Enter Part number: ");
	if (scanf("%d", &number) != 1)
		exit(EXIT_FAILURE);
	i = find_part(number);
	if (i >= 0) {
		printf("Enter change in quantity on hand: ");
		if (scanf("%d", &change) != 1)
			exit(EXIT_FAILURE);
		inventory[i].on_hand += change;
	} else
		printf("Part not found.\n");
}

void print(void) {
	int i;
	
	printf("Part Number      Part Name                  "
				 "Quantity on Hand\n");
	for (i = 0; i < num_parts; i++)
		printf("%7d          %-25s%11d\n", inventory[i].number,
					 inventory[i].name, inventory[i].on_hand);
}
