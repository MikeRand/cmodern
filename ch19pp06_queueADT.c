#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "ch19pp06_queueADT.h"

struct queue_type {
	Item *contents;
	int head;
	int tail;
	int len;
	int size;
};

static void terminate(char *msg) {
	printf("%s\n", msg);
	exit(EXIT_FAILURE);
}

Queue create(int size) {
	Queue new_queue;
	new_queue = malloc(sizeof(struct queue_type));
	if (new_queue == NULL)
		terminate("Error in create: malloc failed.");
	new_queue->contents = malloc(sizeof(Item) * size);
	if (new_queue->contents == NULL) {
		free(new_queue);
		terminate("Error in create: malloc failed.");
	}
	new_queue->size = size;
	new_queue->head = new_queue->size - 1;
	new_queue->tail = new_queue->size - 1;
	new_queue->len = 0;
	return new_queue;
}

void destroy(Queue q) {
	free(q);
}

void make_empty(Queue q) {
	q->head = q->size - 1;
	q->tail = q->size - 1;
	q->len = 0;
}

bool is_empty(Queue q) {
	return q->len == 0;
}

bool is_full(Queue q) {
	return q->len == q->size;
}

void push(Queue q, Item i) {
	if (is_full(q))
		terminate("error in push: queue is full.");
	if (is_empty(q))
		q->head = 0;
	q->tail = (q->tail + 1) % q->size;
	q->contents[q->tail] = i;
	q->len++;
}

Item pop(Queue q) {
	Item i;
	if (is_empty(q))
		terminate("error in pop: queue is empty.");
	i = q->contents[q->head];
	q->head = (q->head + 1) % q->size;
	q->len--;
	return i;
}

Item first(Queue q) {
	if (is_empty(q))
		terminate("error in first: queue is empty.");
	return q->contents[q->head];
}

Item last(Queue q) {
	if (is_empty(q))
		terminate("error in last: queue is empty.");
	return q->contents[q->tail];
}

int length(Queue q) {
	return q->len;
}
