// Caeser cipher

#include <stdio.h>
#include <stdlib.h>

#define MAX_LENGTH 80

int main(void) {
  
  int i, shift;
  char ch;
  char plain_text[MAX_LENGTH];
  char cipher_text[MAX_LENGTH];

  printf("Enter message to be encrypted: ");
  
  for (i = 0; ((ch = getchar()) != '\n'); i++) {
    plain_text[i] = ch;
  }
  plain_text[i] = '\0';

  printf("Enter shift amount (0-25): ");
  scanf("%d", &shift);

  for (i = 0; (ch = plain_text[i]) != '\0'; i++) {
    if (ch >= 'A' && ch <= 'Z') {
      cipher_text[i] = ((ch - 'A') + shift) % 26 + 'A';
    } else if (ch >= 'a' && ch <= 'z') {
      cipher_text[i] = ((ch - 'a') + shift) % 26 + 'a';
    } else {
      cipher_text[i] = ch;
    }
  }
  cipher_text[i] = '\0';

  printf("Encrypted message: ");

  for (i = 0; (ch = cipher_text[i]) != '\0'; i++) {
    printf("%c", ch);
  }
  printf("\n");

  exit(EXIT_SUCCESS);
}
