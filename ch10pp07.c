#include <stdio.h>
#include <stdlib.h>

#define MAX_DIGITS 10

int segments[10][7] = {{1,1,1,0,1,1,1},
											 {0,0,1,0,0,1,0},
											 {1,0,1,1,1,0,1},
											 {1,0,1,1,0,1,1},
											 {0,1,1,1,0,1,0},
											 {1,1,0,1,0,1,1},
											 {0,1,0,1,1,1,1},
											 {1,0,1,0,0,1,0},
											 {1,1,1,1,1,1,1},
											 {1,1,1,1,0,1,0}};

char digits[4][MAX_DIGITS * 4] = {{' '}};

void clear_digits_array(void);
void process_digit(int digit, int position);
void print_digits_array(void);

int main(void) {
	int digit = 0, position = 0;
	char ch;
		
	clear_digits_array();
	printf("Enter a number: ");
	while ((ch = getchar()) != '\n' && position < MAX_DIGITS) {
		switch (ch) {
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				digit = ch - '0';
				process_digit(digit, position++);
				break;
			default:
				break;
		}
	}
	print_digits_array();
	exit(EXIT_SUCCESS);
}

void clear_digits_array(void) {
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < MAX_DIGITS * 4; j++)
			digits[i][j] = ' ';
}

void process_digit(int digit, int position) {
	if (segments[digit][0] == 1)
		digits[0][position * 4 + 1] = '_';
	if (segments[digit][1] == 1)
		digits[1][position * 4 + 0] = '|';
	if (segments[digit][2] == 1)
		digits[1][position * 4 + 2] = '|';
	if (segments[digit][3] == 1)
		digits[1][position * 4 + 1] = '_';
	if (segments[digit][4] == 1)
		digits[2][position * 4 + 0] = '|';
	if (segments[digit][5] == 1)
		digits[2][position * 4 + 2] = '|';
	if (segments[digit][6] == 1)
		digits[2][position * 4 + 1] = '_';
}

void print_digits_array(void) {
	for (int row = 0; row < 4; row++) {
		for (int column = 0; column < MAX_DIGITS * 4; column++) {
			printf("%c", digits[row][column]);
		}
		printf("\n");
	}
}
