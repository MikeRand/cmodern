// Computes pairwise average of 3 numbers.

#include <stdio.h>
#include <stdlib.h>

double average(double a, double b) {
  return (a + b) / 2;
}

int main(void) {
  double x, y, z;

  printf("Enter 3 numbers: ");
  scanf("%lf%lf%lf", &x, &y, &z);
  printf("Average of %g and %g: %g\n", x, y, average(x, y));
  printf("Average of %g and %g: %g\n", y, z, average(y, z));
  printf("Average of %g and %g: %g\n", x, z, average(x, z));
  
  exit(EXIT_SUCCESS);
}
