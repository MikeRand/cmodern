#include <stdio.h>
#include <stdlib.h>

void pay_amount(int dollars, int *twenties, int *tens, int *fives,
								int *ones);

int main(void) {
  
  int dollars, *twenties, *tens, *fives, *ones;
  
  printf("Enter a dollar amount: ");
  if (scanf("%d", &dollars) != 1)
		exit(EXIT_FAILURE);
		
	pay_amount(dollars, twenties, tens, fives, ones);

  printf("$20 bills: %d\n", *twenties);
  printf("$10 bills: %d\n", *tens);
  printf(" $5 bills: %d\n", *fives);
  printf(" $1 bills: %d\n", *ones);
  
  exit(EXIT_SUCCESS);

}

void pay_amount(int dollars, int *twenties, int *tens, int *fives,
								int *ones) {
	
	*twenties = dollars / 20;
  dollars -= *twenties * 20;
  *tens = dollars / 10;
  dollars -= *tens * 10;
  *fives = dollars / 5;
  dollars -= *fives * 5;
  *ones = dollars / 1;
  dollars -= *ones;

}
