#include <stdio.h>
#include <math.h>

int main(void) {
  double x, old_y, y;
  
  printf("Enter a positive number: ");
  scanf("%lf", &x);
  y = 1.0f;

  for (;;) {
    old_y = y;
    y = (old_y + x / old_y) / 2;
    if (fabs(y - old_y) < 0.00001f) {
      break;
    }
  }

  printf("Square root: %f\n", y);
  return 0;
}

  
