// Calculate e

#include <stdio.h>

int main(void) {
  int denominator;
  float e, current_term, epsilon;

  printf("Enter epsilon: ");
  scanf("%f", &epsilon);

  current_term = 1.00f;
  e = current_term;

  for (int i = 1; current_term > epsilon; i++) {
    denominator = 1;
    for (int j = 1; j <= i; j++) {
      denominator *= j;
    }
    current_term = 1.00f / denominator;
    e += current_term;
  }

  printf("Value of e: %f\n", e);
}
    
