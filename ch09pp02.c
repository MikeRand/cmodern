#include <stdio.h>
#include <stdlib.h>

float tax_due(float taxable);

int main(void) {
  
  float taxable;

  printf("Enter taxable income: ");
  scanf("%f", &taxable);

  printf("Taxes due: %.2f\n", tax_due(taxable));

  exit(EXIT_SUCCESS);

}
    
float tax_due(float taxable) {

  float taxes;

  if (taxable < 750.00f) {
    taxes = taxable * 0.01f;
  } else if (taxable < 2250.00f) {
    taxes = 7.50f + (taxable - 750.00f) * 0.02f;
  } else if (taxable < 3750.00f) {
    taxes = 37.50f + (taxable - 2250.00f) * 0.03f;
  } else if (taxable < 5250.00f) {
    taxes = 82.50f + (taxable - 3750.00f) * 0.04f;
  } else if (taxable < 7000.00f) {
    taxes = 142.50f + (taxable - 5250.00f) * 0.05f;
  } else {
    taxes = 230.00f + (taxable - 7000.00f) * 0.06f;
  }

  return taxes;
}
