// Last name first initial

#include <stdio.h>
#include <ctype.h>

int main(void) {
  char c, first_initial;

  printf("Enter a first and last name: ");

  // Burn off initial whitespaces and find first initial.
  while ((first_initial = getchar()) == ' ') {
    ;
  }

  // Burn off rest of first name.
  while ((c = getchar()) != ' ') {
    ;
  }

  // Burn off spaces
  while ((c = getchar()) == ' ') {
    ;
  }
  
  // Print last name
  printf("%c", c); 
  while ((c = getchar()) != ' ') {
    if (c == '\n') {
      break;
    }
    printf("%c", c);
  }

  printf(", %c.\n", first_initial);
  return 0;
}
