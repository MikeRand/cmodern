// Largest of series of numbers

#include <stdio.h>

int main(void) {
  float num, largest;
  largest = -1.00f;

  for (;;) {
    printf("Enter a number: ");
    scanf("%f", &num);
    if (num == 0) {
      break;
    } else if (num > largest) {
      largest = num;
    }
  }
  if (largest > 0) {
    printf("The largest number entered was %f\n", largest);
  } else {
    printf("No positive numbers were entered.\n");
  }
  return 0;
}
    
