ch19pp05: queueclient.c ch19pp05_queueADT.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch19pp05 queueclient.c ch19pp05_queueADT.o

ch19pp05_queueADT.o: ch19pp05_queueADT.c ch19pp05_queueADT.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c ch19pp05_queueADT.c
