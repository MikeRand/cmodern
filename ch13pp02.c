// One-month reminder list

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_REMIND 50
#define MSG_LEN 60

int read_line(char str[], int n);

int main(void) {
	char reminders[MAX_REMIND][3+2+3+3+2+3+MSG_LEN+1];
	char reminder[3+2+3+3+2+3+MSG_LEN+1];
	char month_str[3], day_str[3], hr_str[3], min_str[3],
			 msg_str[MSG_LEN+1];
	int month, day, hr, min, i, j, num_remind = 0;

	for (;;) {
		if (num_remind == MAX_REMIND) {
			printf("-- No space left --\n");
			break;
		}

		printf("Enter month/day, time (24-hr hh:mm) and reminder: ");
		
		if (scanf("%d/", &month) != 1)
			exit(EXIT_FAILURE);
		if (month == 0)
			break;
		if (month < 1 || month > 12) {
			printf("ERROR: Month must be between 1 and 12.\n");
			continue;
		}
		sprintf(month_str, "%d", month);
		
		if (scanf("%2d", &day) != 1)
			exit(EXIT_FAILURE);
		if (day < 0 || day > 31) {
			printf("ERROR: Day must be between 0 and 31.\n");
			continue;
		}
		sprintf(day_str, "%2d", day);
		
		if (scanf("%d:%2d", &hr, &min) != 2)
			exit(EXIT_FAILURE);
		if (hr > 23 || hr < 0) {
			printf("ERROR: hour must be between 0 and 23.\n");
			continue;
		}
		else if (min > 59 || min < 0) {
			printf("ERROR: minute must be between 0 and 59.\n");
			continue;
		}
		sprintf(hr_str, "%d", hr);
		sprintf(min_str, "%d", min);
		
		read_line(msg_str, MSG_LEN);
		
		strcpy(reminder, month_str);
		strcat(reminder, "/");
		strcat(reminder, day_str);
		strcat(reminder, hr_str);
		strcat(reminder, ":");
		strcat(reminder, min_str);
		strcat(reminder, msg_str);
		
		for (i = 0; i < num_remind; i++)
			if (strcmp(reminder, reminders[i]) < 0)
				break;
		for (j = num_remind; j > i; j--)
			strcpy(reminders[j], reminders[j-1]);
		
		strcpy(reminders[i], reminder);
				
		num_remind++;
	}
	
	printf("\nMon/Day Time Reminder\n");
	for (i = 0; i < num_remind; i++)
		printf(" %s\n", reminders[i]);
		
	exit(EXIT_SUCCESS);
}

int read_line(char str[], int n) {
	int ch, i = 0;
	
	while ((ch = getchar()) != '\n')
		if (i < n)
			str[i++] = ch;
	str[i] = '\0';
	return i;
}
