/* Declare structure and check offsets. */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

struct s {
  char a;
  int b[2];
  float c;
};

int main(void) {
  size_t size;
  size_t aoff, boff, coff;
  
  size = sizeof(struct s);
  aoff = offsetof(struct s, a);
  boff = offsetof(struct s, b);
  coff = offsetof(struct s, c);
  
  printf("sizeof s: %zd\n", size);
  printf("offset of a: %zd\n", aoff);
  printf("offset of b: %zd\n", boff);
  printf("offset of c: %zd\n", coff);
  
  if (coff + sizeof(float) == size)
    printf("No holes in structure.\n");
  else {
    if (boff != sizeof(char))
      printf("Hole of size %zd before b.\n", boff - sizeof(char));
    if (coff != boff + (2 * sizeof(int)))
      printf("Hole of size %zd before c.\n", coff-boff-sizeof(int)*2);
  }
  
  exit(EXIT_SUCCESS);
}
  
