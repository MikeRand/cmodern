#include <stdio.h>

int main(void) {
  float balance, rate, pmt, mrate;
  
  printf("Enter amount of loan: ");
  scanf("%f", &balance);
  printf("Enter interest rate: ");
  scanf("%f", &rate);
  printf("Enter monthly payment: ");
  scanf("%f", &pmt);

  mrate = rate / 1200.f;

  balance = balance * (1 + mrate) - pmt;
  printf("Balance remaing after first payment: $%.2f\n", balance);
  balance = balance * (1 + mrate) - pmt;
  printf("Balance remaing after second  payment: $%.2f\n", balance);
  balance = balance * (1 + mrate) - pmt;
  printf("Balance remaing after third  payment: $%.2f\n", balance);

  return 0;
}
