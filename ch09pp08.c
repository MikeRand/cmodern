// Craps

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int roll_dice(void);
bool play_game(void);

int main(void) {

  int wins = 0, losses = 0;
  bool result;
  char play_again;

  srand((unsigned) time(NULL));
  
  for (;;) {

    result = play_game();
    if (result) {
      printf("You win!\n");
      wins++;
    } else {
      printf("You lose!\n");
      losses++;
    }

    printf("\nPlay again (y/n)? ");
    scanf(" %c", &play_again);

    if (toupper(play_again) != 'Y') {
      printf("Wins: %d   Losses: %d\n", wins, losses);
      break;
    }
  }

  exit(EXIT_SUCCESS);
}

int roll_dice(void) {

  int d1, d2, d;
  d1 = (rand() % 6) + 1;
  d2 = (rand() % 6) + 1;
  d = d1 + d2;
  printf("You rolled: %d\n", d);
  return d;

}

bool play_game(void) {
  
  int v, point;

  v = roll_dice();
  
  if (v == 2 || v == 3 || v == 12)
    return false;
  else if (v == 7 || v == 11)
    return true;
  else {
    printf("Your point is %d\n", v);
    point = v;
    for (;;) {
      v = roll_dice();
      if (v == 7)
	return false;
      else if (v == point)
	return true;
    }
  }
}
