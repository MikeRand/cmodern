#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "queueADT.h"

#define QUEUE_SIZE 100

struct queue_type {
	Item contents[QUEUE_SIZE];
	int head;
	int tail;
	int len;
};

static void terminate(char *msg) {
	printf("%s\n", msg);
	exit(EXIT_FAILURE);
}

Queue create(void) {
	Queue new_queue;
	new_queue = malloc(sizeof(struct queue_type));
	if (new_queue == NULL)
		terminate("Error in create: malloc failed.");
	new_queue->head = QUEUE_SIZE - 1;
	new_queue->tail = QUEUE_SIZE - 1;
	new_queue->len = 0;
	return new_queue;
}

void destroy(Queue q) {
	free(q);
}

void make_empty(Queue q) {
	q->head = QUEUE_SIZE - 1;
	q->tail = QUEUE_SIZE - 1;
	q->len = 0;
}

bool is_empty(Queue q) {
	return q->len == 0;
}

bool is_full(Queue q) {
	return q->len == QUEUE_SIZE;
}

void push(Queue q, Item i) {
	if (is_full(q))
		terminate("error in push: queue is full.");
	if (is_empty(q))
		q->head = 0;
	q->tail = (q->tail + 1) % QUEUE_SIZE;
	q->contents[q->tail] = i;
	q->len++;
}

Item pop(Queue q) {
	Item i;
	if (is_empty(q))
		terminate("error in pop: queue is empty.");
	i = q->contents[q->head];
	q->head = (q->head + 1) % QUEUE_SIZE;
	q->len--;
	return i;
}

Item first(Queue q) {
	if (is_empty(q))
		terminate("error in first: queue is empty.");
	return q->contents[q->head];
}

Item last(Queue q) {
	if (is_empty(q))
		terminate("error in last: queue is empty.");
	return q->contents[q->tail];
}

int length(Queue q) {
	return q->len;
}
