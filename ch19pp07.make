ch19pp07: queueclient.c ch19pp07_queueADT.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch19pp07 queueclient.c ch19pp07_queueADT.o

ch19pp07_queueADT.o: ch19pp07_queueADT.c ch19pp07_queueADT.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c ch19pp07_queueADT.c
