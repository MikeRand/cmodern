// Last name first initial

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX_LENGTH 20

int main(void) {
  char c, first_initial;
  char last_name[MAX_LENGTH];

  printf("Enter a first and last name: ");

  // Burn off initial whitespaces and find first initial.
  while ((first_initial = getchar()) == ' ') {
    ;
  }

  // Burn off rest of first name.
  while ((c = getchar()) != ' ') {
    ;
  }

  // Burn off spaces
  while ((c = getchar()) == ' ') {
    ;
  }
  
  // Store last name
  last_name[0] = c;
  for (int i = 1; (c = getchar()) != ' '; i++) {
    if (c == '\n') {
      last_name[i] = '\0';
      break;
    } else {
      last_name[i] = c;
    }
  }
	 
  printf("You entered the name: ");
  
  for (int i = 0; last_name[i] != '\0'; i++) {
    printf("%c", last_name[i]);
  }
	 
  printf(", %c.\n", first_initial);
  
  exit(EXIT_SUCCESS);
}
