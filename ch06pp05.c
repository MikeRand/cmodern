// reverse digits

#include <stdio.h>

int main(void) {
  int number, rev_number;
  rev_number = 0;

  printf("Enter a number: ");
  scanf("%d", &number);
  
  do {
    rev_number = rev_number * 10 + number % 10;
    number /= 10;
  } while (number > 0);

  printf("The reversal is: %d\n", rev_number);

  return 0;
}
