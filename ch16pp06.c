// Lesser of 2 dates

#include <stdio.h>
#include <stdlib.h>

typedef struct date {
	int year;
	int month;
	int day;
} date;

int compare_dates(const date d1, const date d2);

int main(void) {
	date d1, d2;

	printf("Enter first date (mm/dd/yy): ");
	if (scanf("%d/%d/%d", &d1.month, &d1.day, &d1.year) != 3)
		exit(EXIT_FAILURE);
	printf("Enter second date (mm/dd/yy): ");
	if (scanf("%d/%d/%d", &d2.month, &d2.day, &d2.year) != 3)
		exit(EXIT_FAILURE);

	if (compare_dates(d1, d2)) {
		printf("%d/%d/%.2d is earlier than %d/%d/%.2d\n",
					 d1.month, d1.day, d1.year, d2.month, d2.day, d2.year);
	} else {
		printf("%d/%d/%.2d is earlier than %d/%d/%.2d\n",
					 d2.month, d2.day, d2.year, d1.month, d1.day, d1.year);  }

	exit(EXIT_SUCCESS);
}

int compare_dates(const date d1, const date d2) {
	return (d1.year * 360 + d1.month * 30 + d1.day) - \
				 (d2.year * 360 + d2.month * 30 + d1.day);
} 
