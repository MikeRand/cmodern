#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_SENTENCE_SIZE 1024

int main(void) {
  
  int sentence_size = 0;
  char ch;
  char sentence[MAX_SENTENCE_SIZE];

  printf("Enter message: ");
  
  while ((ch = getchar()) != '\n') {
    sentence[sentence_size++] = ch;
  }
  sentence[sentence_size] = '\0';

  printf("In B1FF-speak: ");

  for (int i = 0; i < sentence_size; i++) {
    switch(toupper(sentence[i])) {
      case 'A':
	ch = '4';
	break;
      case 'B':
	ch = '8';
	break;
      case 'E':
	ch = '3';
	break;
      case 'I':
	ch = '1';
	break;
      case 'O':
	ch = '0';
	break;
      case 'S':
	ch = '5';
	break;
      default:
	ch = toupper(sentence[i]);
    }
    printf("%c", ch);
  }
  for (int i = 0; i < 10; i++) {
    printf("!");
  }

  printf("\n");
  exit(EXIT_SUCCESS);
}
	

  
