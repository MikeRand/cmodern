// Sum a series of numbers

#include <stdio.h>

int main(void) {
  
  int n, sum;
  sum = 0;
  printf("This program sums a series of integers.\n");
  printf("Enter integers (0 to exit): ");
  scanf("%d", &n);
  while (n > 0) {
    sum += n;
    scanf("%d", &n);
  }
  printf("Sum: %d\n", sum);
  return 0;
}
    
    
  
  
