ch17pp02: ch17pp02.c readline.o
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -o ch17pp02 ch17pp02.c readline.o

readline.o: readline.c readline.h
		gcc -Wall -Wextra -Wfloat-equal -Wundef -Wshadow \
		-Wpointer-arith -Wcast-align -Wstrict-prototypes \
		-pedantic -std=c99 -O3 -g -c readline.c
